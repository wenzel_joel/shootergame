// Fill out your copyright notice in the Description page of Project Settings.

#include "SCharacter.h"
#include "../Components/SCharacterMovementComponent.h"
#include "../Weapon/SBaseWeapon.h"
#include "../Components/SHealthComponent.h"
#include "../Components/InventoryComponent.h"
#include "../Components/SInteractComponent.h"
#include "../CoopGame.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Components/CapsuleComponent.h"
#include "DrawDebugHelpers.h"



// Sets default values
ASCharacter::ASCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<USCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	HealthComponent = CreateDefaultSubobject<USHealthComponent>(TEXT("HealthComponent"));
	Inventory = CreateDefaultSubobject<UInventoryComponent>(TEXT("WeaponInventory"));
	InteractComponent = CreateDefaultSubobject<USInteractComponent>(TEXT("InteractComponent"));

	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;

	bIsAiming = false;
	bIsInCover = false;

}

void ASCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ASCharacter, CurrentItem, COND_OwnerOnly);
	DOREPLIFETIME(ASCharacter, bIsAiming);
	DOREPLIFETIME(ASCharacter, bHasDied);
}

// Called when the game starts or when spawned
void ASCharacter::BeginPlay()
{
	Super::BeginPlay();

	HealthComponent->OnHealthChanged.AddDynamic(this, &ASCharacter::OnHealthChanged);
	Inventory->OnItemSelected.AddDynamic(this, &ASCharacter::OnItemSelected);
	Inventory->OnItemUnselected.AddDynamic(this, &ASCharacter::OnItemUnselected);

	//equip default weapon
	if (Role == ROLE_Authority)
	{
		Equip(0);
	}

}

void ASCharacter::MoveForward(float Val)
{
	if (Val < 0 && bIsInCover)
	{
		LeaveCover();
	}
	AddMovementInput(GetActorForwardVector(), Val);
}

void ASCharacter::MoveRight(float Val)
{
	AddMovementInput(GetActorRightVector(), Val);
}

void ASCharacter::BeginCrouch()
{
	Crouch();
}

void ASCharacter::EndCrouch()
{
	UnCrouch();
}

void ASCharacter::StartJump()
{
	Jump();
}

void ASCharacter::StopJump()
{
	StopJumping();
}

void ASCharacter::EnterCover()
{
	USCharacterMovementComponent* MovementComp = Cast<USCharacterMovementComponent>(GetMovementComponent());
	if (MovementComp && !bIsInCover)
	{
		MovementComp->RequestEnterCover();
		bIsInCover = true;

	}
}

void ASCharacter::LeaveCover()
{
	USCharacterMovementComponent* MovementComp = Cast<USCharacterMovementComponent>(GetMovementComponent());
	if (MovementComp && bIsInCover)
	{
		MovementComp->RequestLeaveCover();
		bIsInCover = false;
	}
}

void ASCharacter::StartAiming()
{
	if (Role < ROLE_Authority)
	{
		Server_StartAiming();
	}

	ASBaseWeapon* Weapon = Cast<ASBaseWeapon>(CurrentItem);
	if (Weapon)
	{
		bIsAiming = true;
		Weapon->RequestStartAim();
	}
}

void ASCharacter::StopAiming()
{
	if (Role < ROLE_Authority)
	{
		Server_StopAiming();
	}
	bIsAiming = false;

	ASBaseWeapon* Weapon = Cast<ASBaseWeapon>(CurrentItem);

	if (Weapon  && IsLocallyControlled())
	{
		Weapon->RequestStopAim();
	}
}

void ASCharacter::StartFire()
{
	if (CurrentItem)
	{
		CurrentItem->OnUsePressed();
	}
}

void ASCharacter::StopFire()
{
	if (CurrentItem)
	{
		CurrentItem->OnUseReleased();
	}
}

void ASCharacter::RequestReload()
{
	if (ASBaseWeapon* Weapon = Cast<ASBaseWeapon>(CurrentItem))
	{
		Weapon->RequestReload();
	}
}

void ASCharacter::ToggleFireMode()
{
	if (ASBaseWeapon* Weapon = Cast<ASBaseWeapon>(CurrentItem))
	{
		Weapon->ToggleFiringMode();
	}
}

void ASCharacter::StartInteract()
{
	InteractComponent->StartInteract();
}

void ASCharacter::StopInteract()
{
	InteractComponent->StopInteract();
}

void ASCharacter::Equip(uint8 Index)
{
	//TODO--- maybe swap weapons on client and ignore on rep, this way weapon swap isnt delayed by lag
	if (Role < ROLE_Authority)
	{
		Server_Equip(Index);
	}
	if (Inventory)
	{
		Inventory->SelectItem(Index);
	}
}

void ASCharacter::Server_Equip_Implementation(uint8 Index)
{
	Equip(Index);
}

bool ASCharacter::Server_Equip_Validate(uint8 Index)
{
	return true;
}

void ASCharacter::EquipSlot1()
{
	Equip(0);
}

void ASCharacter::EquipSlot2()
{
	Equip(1);
}

void ASCharacter::EquipSlot3()
{
	Equip(2);
}

void ASCharacter::EquipSlot4()
{
	Equip(3);
}

void ASCharacter::Server_StartAiming_Implementation()
{
	StartAiming();
}

bool ASCharacter::Server_StartAiming_Validate()
{
	return true;
}

void ASCharacter::Server_StopAiming_Implementation()
{
	StopAiming();
}

bool ASCharacter::Server_StopAiming_Validate()
{
	return true;
}

void ASCharacter::OnHealthChanged(class USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, class AActor* DamageCauser)
{
	if (Health <= 0.f && !bHasDied)
	{
		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		bHasDied = true;

		DetachFromControllerPendingDestroy();
		SetLifeSpan(10.f);
	}
}

void ASCharacter::OnItemSelected(uint8 SelectedSlot, ASInventoryItem* NewItem)
{
	CurrentItem = NewItem;
}

void ASCharacter::OnItemUnselected(uint8 UnselectedSlot, ASInventoryItem* OldWeapon)
{

}

// Called every frame
void ASCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ASCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &ASCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookRight", this, &ASCharacter::AddControllerYawInput);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ASCharacter::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ASCharacter::EndCrouch);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ASCharacter::StartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ASCharacter::StopJump);

	PlayerInputComponent->BindAction("Cover", IE_Pressed, this, &ASCharacter::EnterCover);

	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &ASCharacter::StartAiming);
	PlayerInputComponent->BindAction("Aim", IE_Released, this, &ASCharacter::StopAiming);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASCharacter::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ASCharacter::StopFire);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ASCharacter::StartInteract);
	PlayerInputComponent->BindAction("Interact", IE_Released, this, &ASCharacter::StopInteract);

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ASCharacter::RequestReload);
	PlayerInputComponent->BindAction("ToggleFireMode", IE_Pressed, this, &ASCharacter::ToggleFireMode);

	PlayerInputComponent->BindAction("EquipSlot1", IE_Released, this, &ASCharacter::EquipSlot1);
	PlayerInputComponent->BindAction("EquipSlot2", IE_Released, this, &ASCharacter::EquipSlot2);
	PlayerInputComponent->BindAction("EquipSlot3", IE_Released, this, &ASCharacter::EquipSlot3);
	PlayerInputComponent->BindAction("EquipSlot4", IE_Released, this, &ASCharacter::EquipSlot4);

}
