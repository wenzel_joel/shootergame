// Fill out your copyright notice in the Description page of Project Settings.

#include "SPlayerCameraManager.h"
#include "SCharacter.h"




ASPlayerCameraManager::ASPlayerCameraManager()
{
	NormalFOV = 90.0f;
	TargetingFOV = 55.0f;//20.0f is a good value for sniping
	ViewPitchMin = -87.0f;
	ViewPitchMax = 87.0f;
	//bAlwaysApplyModifiers = true;
	CameraStyle = FName(TEXT("FreeCam"));
	FreeCamDistance = 200.f;

	NormalCamDistance = FreeCamDistance;
	SprintingCamDistance = NormalCamDistance + 100.f;

	TargetingTransitionSpeed = 10.f;
	SprintingTransitionSpeed = 10.f;
}

void ASPlayerCameraManager::UpdateCamera(float DeltaTime)
{
	Super::UpdateCamera(DeltaTime);

	ASCharacter* MyCharacter = Cast<ASCharacter>(GetViewTarget());
	if (MyCharacter)
	{
		float DesiredFOV = MyCharacter->IsAiming() ? TargetingFOV : NormalFOV;
		DefaultFOV = FMath::FInterpTo(DefaultFOV, DesiredFOV, DeltaTime, TargetingTransitionSpeed);
	}

}
