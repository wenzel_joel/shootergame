// Fill out your copyright notice in the Description page of Project Settings.

#include "SPlayerController.h"
#include "GameFramework/Pawn.h"
#include "../UI/SHUD.h"



ASPlayerController::ASPlayerController()
{
	bIsInventoryDisplayed = false;
}

void ASPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("InventoryMenu", IE_Pressed, this, &ASPlayerController::ToggleInventoryMenu);
}

void ASPlayerController::BeginPlay()
{
	Super::BeginPlay();

	SetPlayerState(GetDefaultPlayerState());
}

void ASPlayerController::InstigatedAnyDamage(float Damage, const class UDamageType* DamageType, class AActor* DamagedActor, class AActor* DamageCauser)
{
	Super::InstigatedAnyDamage(Damage, DamageType, DamagedActor, DamageCauser);
	//show damage on screen
	if (IsLocalController())
	{
		if (DamagedActor && DamagedActor->IsA(APawn::StaticClass()))
		{
			FVector2D ActorScreenLocation;

			ProjectWorldLocationToScreen(DamagedActor->GetActorLocation(), ActorScreenLocation);

			//offset the Y so it doesn't block cursor, update this for scaling.  100 pixels 
			//TODO- doesnt take UI scaling into account
			ActorScreenLocation.Y += -100;

			ASHUD* HUD = Cast<ASHUD>(GetHUD());

			if (HUD)
			{
				//scale font size based on distance to target
				//float DistanceScaling = (GetPawn()->GetActorLocation() - DamagedActor->GetActorLocation()).Size();
				//float FontScale = FMath::Clamp((IndicatorScalingDistance / DistanceScaling), .2f, 1.f);

				int32 CritThrow = FMath::RandHelper(10);
				HUD->ShowDamageIndicator(Damage, ActorScreenLocation, 15.f, CritThrow != 1 ? FColor::White : FColor::Red);
			}
		}
	}
	else
	{
		Client_InstigatedAnyDamage(Damage, DamageType, DamagedActor, DamageCauser);
	}
}

EPlayerState::Type ASPlayerController::GetDefaultPlayerState()
{
	//TODO.. get more specific here, eventually this will be warmp up.
	//however if a player joins mid match, it may be gameplay or Spectating

	return EPlayerState::Gameplay;
}

void ASPlayerController::SetPlayerState(const EPlayerState::Type)
{
	
}

void ASPlayerController::ToggleInventoryMenu()
{
	ASHUD* HUD = Cast<ASHUD>(GetHUD());
	if (!HUD) { return; }

	if (bIsInventoryDisplayed)
	{
		HUD->ShowGameplayWidget();
		bIsInventoryDisplayed = false;
	}
	else
	{
		HUD->ShowInventoryWidget();
		bIsInventoryDisplayed = true;
	}
}

void ASPlayerController::Client_InstigatedAnyDamage_Implementation(float Damage, const class UDamageType* DamageType, class AActor* DamagedActor, class AActor* DamageCauser)
{
	InstigatedAnyDamage(Damage, DamageType, DamagedActor, DamageCauser);
}
