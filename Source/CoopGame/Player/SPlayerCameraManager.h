// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/PlayerCameraManager.h"
#include "SPlayerCameraManager.generated.h"

/**
 * 
 */
UCLASS()
class COOPGAME_API ASPlayerCameraManager : public APlayerCameraManager
{
	GENERATED_BODY()

	/** normal FOV */
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float NormalFOV;

	/** targeting FOV */
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float TargetingFOV;

	UPROPERTY(EditDefaultsOnly, Category = "Camera", meta = (ClampMin = 0.1, ClampMax = 100))
	float TargetingTransitionSpeed;

	/** normal free cam distance*/
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float NormalCamDistance;

	/** boosting cam distance*/
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float SprintingCamDistance;

	UPROPERTY(EditDefaultsOnly, Category = "Camera", meta = (ClampMin = 0.1, ClampMax = 100))
	float SprintingTransitionSpeed;

	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	TSubclassOf<UCameraShake> DefaultCameraShake;
	
public:
	ASPlayerCameraManager();

	virtual void UpdateCamera(float DeltaTime) override;
	
	
};
