// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SPlayerController.generated.h"

namespace EPlayerState
{
	enum Type
	{
		Warmup, //match hasn't started and no input is allowed
		Gameplay, //player is moving around and shooting
		Down, //player has pawn, but movement disabled
		Spectating, //player has no pawn, waiting to respawn
	};
}

/**
 * 
 */
UCLASS()
class COOPGAME_API ASPlayerController : public APlayerController
{
	GENERATED_BODY()


public:
	ASPlayerController();

	virtual void SetupInputComponent();

protected:
	virtual void BeginPlay() override;

	virtual void InstigatedAnyDamage(float Damage, const class UDamageType* DamageType, class AActor* DamagedActor, class AActor* DamageCauser) override;
	
	UFUNCTION(Client, Reliable)
	void Client_InstigatedAnyDamage(float Damage, const class UDamageType* DamageType, class AActor* DamagedActor, class AActor* DamageCauser);


	////////////////////////////////////
	//STATE
	EPlayerState::Type GetDefaultPlayerState();

	void SetPlayerState(const EPlayerState::Type);

	////////////////////////////////////
	//UI
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void ToggleInventoryMenu();

protected:
	uint8 bIsInventoryDisplayed;

	EPlayerState::Type CurrentState;

};
