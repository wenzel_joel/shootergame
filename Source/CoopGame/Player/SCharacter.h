// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SCharacter.generated.h"

UCLASS()
class COOPGAME_API ASCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASCharacter(const class FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Character")
	FORCEINLINE bool IsAiming() { return bIsAiming; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Character")
	FORCEINLINE class UInventoryComponent* GetInventory() { return Inventory; }


protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character")
	class USHealthComponent* HealthComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character")
	class UInventoryComponent* Inventory;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character")
	class USInteractComponent* InteractComponent;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Health")
	bool bHasDied;

	UPROPERTY(Replicated)
	bool bIsAiming;

	uint8 bIsInCover;



	/////////////////////////////
	//INVENTORY

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Inventory")
	class ASInventoryItem* CurrentItem;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	/////////////////////////////
	//PLAYER INPUT

	virtual void MoveForward(float Val);
	virtual void MoveRight(float Val);
	
	virtual void BeginCrouch();
	virtual void EndCrouch();

	virtual void StartJump();
	virtual void StopJump();

	virtual void EnterCover();
	virtual void LeaveCover();

	virtual void StartAiming();
	virtual void StopAiming();

	virtual void StartFire();
	virtual void StopFire();

	virtual void RequestReload();
	virtual void ToggleFireMode();

	virtual void StartInteract();
	virtual void StopInteract();

	virtual void EquipSlot1();
	virtual void EquipSlot2();
	virtual void EquipSlot3();
	virtual void EquipSlot4();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_StartAiming();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_StopAiming();


	///////////////////////////
	//WEAPON

	virtual void Equip(uint8 Index);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Equip(uint8 Index);

	//////////////////////////////
	//COMPONENT CALLBACKS

	UFUNCTION()
	void OnHealthChanged(class USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, class AActor* DamageCauser);

	/** Only called on the server, client logic can hook through OnRep_CurrentWeapon */
	UFUNCTION()
	void OnItemSelected(uint8 SelectedSlot, ASInventoryItem* NewItem);

	UFUNCTION()
	void OnItemUnselected(uint8 UnselectedSlot, ASInventoryItem* OldItem);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;	
};
