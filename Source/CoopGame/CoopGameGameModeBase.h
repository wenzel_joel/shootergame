// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CoopGameGameModeBase.generated.h"

enum class EWaveState : uint8;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnPawnKill, class APawn*, KilledPawn, class AController*, Instigator);

/**
 * 
 */
UCLASS()
class COOPGAME_API ACoopGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ACoopGameGameModeBase();

	UPROPERTY(BlueprintAssignable)
	FOnPawnKill OnPawnKilled;

	/*Called when pawns are killed*/
	void HandleKill(AController* KillingPlayer, AController* KilledPlayer, APawn* KilledPawn);

	virtual void StartPlay() override;

	virtual void Tick(float DeltaSeconds) override;

protected:
	FTimerHandle BotSpawnTimer;
	FTimerHandle TimerHandle_NextWaveStart;

	/*Bots to spawn in current wave*/
	UPROPERTY(EditDefaultsOnly, Category = "GameMode")
	int32 NumBotsToSpawn;

	uint8 CurrentWave;

	UPROPERTY(EditDefaultsOnly, Category = "GameMode")
		float TimeBetweenWaves;

protected:
	/*Hook for BP to spawn single bot*/
	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void SpawnNewBot();

	void SpawnBotTimerElapsed();

	void StartWave();

	void EndWave();

	void PrepareForNextWave();

	void CheckWaveState();

	void CheckPlayersAlive();

	void EndGame();

	void SetWaveState(EWaveState NewState);

	void RespawnDeadPlayers();
};
