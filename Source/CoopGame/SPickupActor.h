// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SPickupActor.generated.h"

UCLASS()
class COOPGAME_API ASPickupActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASPickupActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Setup")
	class USphereComponent* SphereComp;
	
	UPROPERTY(VisibleAnywhere, Category = "Setup")
	class UDecalComponent* DecalComp;

	UPROPERTY(EditAnywhere, Category = "Setup")
	TSubclassOf<class ASPowerupActor> PowerupClass;

	class ASPowerupActor* PowerupInstance;

	UPROPERTY(EditAnywhere, Category = "Setup")
	float CooldownDuration;

	FTimerHandle TimerHandle_RespawnTimer;

	UFUNCTION()
	void Respawn();

public:
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	virtual void NotifyActorEndOverlap(AActor* OtherActor) override;
};
