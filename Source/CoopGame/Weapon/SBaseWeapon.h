// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Inventory/SInventoryItem.h"
#include "SBaseWeapon.generated.h"

namespace EWeaponState
{
	enum Type
	{
		Idle,
		Firing,
		Reloading,
		Equipping,
	};
}

UENUM(BlueprintType)
enum class EFireMode : uint8
{
	SemiAuto	UMETA(DisplayName = "Semi Auto"),
	Burst		UMETA(DisplayName = "Burst"),
	Auto		UMETA(DisplayName = "Auto")
};

/**
 * Struct representing an individual firing mode for a weapon.  IE burst fire, single shot etc.
 */
USTRUCT(BlueprintType)
struct FFiringMode
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	EFireMode FireMode;

	UPROPERTY(EditDefaultsOnly)
	float TimeBetweenShots;

	/**  Angle in degrees for weapon spread.  the closer to 0, the more accurate*/
	UPROPERTY(EditDefaultsOnly)
	float SpreadAngle;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class USoundBase* FireSound;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class USoundBase* FireSoundEnd;

	FFiringMode()
	{
		FireMode = EFireMode::Auto;
		FireSound = nullptr;
		FireSoundEnd = nullptr;
		SpreadAngle = 0.f;
		TimeBetweenShots = .5f;
	}
};

USTRUCT(BlueprintType)
struct FWeaponData
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	int32 MaxAmmo;

	UPROPERTY(EditDefaultsOnly)
	int32 MaxAmmoInMag;

	UPROPERTY(EditDefaultsOnly)
	int32 BaseDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponData")
	TArray<FFiringMode> FireModes;

	/*
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<class UCrosshairWidget> CrosshairClass;
		*/
	FWeaponData()
	{
		/*Add the default fire mode to ensure we can shoot*/
		FireModes.Add(FFiringMode());
	}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStartReload, float, Duration);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStopReload, uint8, bWasCompleted);

UCLASS(abstract)
class COOPGAME_API ASBaseWeapon : public ASInventoryItem
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ASBaseWeapon();

	FOnStartReload OnStartReload;
	FOnStopReload OnStopReload;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "WeaponData")
	FORCEINLINE FWeaponData& GetWeaponData() { return WeaponData; }
	

	/** Inventory Item Interface */
	virtual void OnSelect() override;
	virtual void OnUnselect() override;
	virtual void OnPickup() override;
	virtual void OnDrop() override;
	virtual void OnUsePressed() override;
	virtual void OnUseReleased() override;

protected:
	//////////////////////////////////////////
	//DATA

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Setup")
	class USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditDefaultsOnly, Category = "WeaponData")
	FName MuzzleSocketName = TEXT("MuzzleLocation");

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Weapon")
	FVector GetWeaponMuzzleLocation();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Weapon")
	FTransform GetWeaponMuzzleTransform();

	UPROPERTY(EditDefaultsOnly, Category = "WeaponData")
	FWeaponData WeaponData;

	UPROPERTY(EditDefaultsOnly, Category = "WeaponData")
	bool bFireOnRelease;


	//////////////////////////////////////////
	//FIRING
	FTimerHandle FiringTimer;
	
	uint8 bIsFiring;

	UPROPERTY(Replicated)
	int32 CurrentAmmoInMag;

	UPROPERTY(Replicated)
	int32 CurrentAmmo;

	uint8 bIsAiming;

	uint8 CurrentFiringMode;

	EWeaponState::Type CurrentState;


	//////////////////////////////////////////
	//RELOADING
	FTimerHandle ReloadTimer;

	UPROPERTY(ReplicatedUsing = OnRep_IsReloading)
	uint8 bIsReloading;


	//////////////////////////////////////////
	//EFFECTS AND SOUND
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	TSubclassOf<class ASImpactEffect> ImpactEffectClass;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class USoundBase* ReloadSound;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class USoundBase* EquipSound;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class USoundBase* ToggleFireModeSound;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class UAnimMontage* WeaponEquipAnim;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class UAnimMontage* ReloadAnim;

	UPROPERTY()
	class UAudioComponent* FireAC;


	//////////////////////////////////////////
	//UI
	//UPROPERTY()
	//class UCrosshairWidget* Crosshair;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(EEndPlayReason::Type Reason) override;

	/**
	* Actually handles the commencing of fire.
	* Initiates the fire loop, begins and looped sounds
	*/
	virtual void StartFiring();

	/**
	* Stops the firing loop, kills the sounds
	*/
	virtual void StopFiring();

	/**
	* Actually fire a round from the gun.
	* Children should overwrite with their custom logic.
	*/
	virtual void Fire();

	virtual void StartReload();

	UFUNCTION()
	void OnRep_IsReloading();

	void SimulateReload();


	virtual bool CanFire();

	virtual bool CanReload();

	UFUNCTION()
	virtual void Reload();

	virtual void UseAmmo();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_UseAmmo();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_StartReload();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Firing")
	FORCEINLINE bool IsFiring() { return bIsFiring; }

	bool GetShootDirection(FVector& OutLocation, FVector& OutDirection);

	virtual float PlayAnimation(UAnimMontage* AnimToPlay);

	const FFiringMode& GetCurrentFiringMode();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/**
	* Manages state, sees if the player can fire.
	* Notifies listeners that we are about to fire
	*/
	virtual void RequestStartFire();

	/**
	* Manages state, notifies listeners we are done firing.
	*/
	virtual void RequestStopFire();

	/**
	 * Toggles the current firing mode one mode forward
	 */
	UFUNCTION(BlueprintCallable, Category = "Firing")
	void ToggleFiringMode();

	void RequestStartAim();
	void RequestStopAim();

	UFUNCTION(BlueprintCallable, Category = "Firing")
		virtual void RequestReload();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Firing")
		FORCEINLINE int32 GetCurrentAmmoInMag() { return CurrentAmmoInMag; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Firing")
		FORCEINLINE int32 GetCurrentAmmo() { return CurrentAmmo; }

	virtual void OnEquip();
	virtual void OnUnequip();


};