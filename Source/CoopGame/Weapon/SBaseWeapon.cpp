// Fill out your copyright notice in the Description page of Project Settings.

#include "SBaseWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Animation/AnimInstance.h"
#include "Net/UnrealNetwork.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundBase.h"
#include "../Player/SCharacter.h"

// Sets default values
ASBaseWeapon::ASBaseWeapon()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	SetRootComponent(WeaponMesh);

	bIsFiring = false;
	bIsAiming = false;
	bFireOnRelease = false;

	SetReplicates(true);
	NetUpdateFrequency = 66.f;
	MinNetUpdateFrequency = 33.f;

	CurrentFiringMode = 0;
	CurrentState = EWeaponState::Idle;
	bIsReloading = false;

}

void ASBaseWeapon::OnSelect()
{
	Super::OnSelect();

	OnEquip();
}

void ASBaseWeapon::OnUnselect()
{
	Super::OnUnselect();

	OnUnequip();
}

void ASBaseWeapon::OnPickup()
{
	//TODO
}

void ASBaseWeapon::OnDrop()
{
	Super::OnDrop();

}

void ASBaseWeapon::OnUsePressed()
{
	RequestStartFire();
}

void ASBaseWeapon::OnUseReleased()
{
	RequestStopFire();
}

void ASBaseWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ASBaseWeapon, bIsReloading, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(ASBaseWeapon, CurrentAmmoInMag, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ASBaseWeapon, CurrentAmmo, COND_OwnerOnly);
}


// Called when the game starts or when spawned
void ASBaseWeapon::BeginPlay()
{
	Super::BeginPlay();

	CurrentAmmoInMag = WeaponData.MaxAmmoInMag;
	CurrentAmmo = WeaponData.MaxAmmo;
}

void ASBaseWeapon::EndPlay(EEndPlayReason::Type Reason)
{
	if (FireAC)
	{
		FireAC->FadeOut(.1, 0.f);
	}
	Super::EndPlay(Reason);
}

// Called every frame
void ASBaseWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FTransform ASBaseWeapon::GetWeaponMuzzleTransform()
{
	return WeaponMesh ? WeaponMesh->GetSocketTransform(MuzzleSocketName) : FTransform();
}



FVector ASBaseWeapon::GetWeaponMuzzleLocation()
{
	return WeaponMesh ? WeaponMesh->GetSocketLocation(MuzzleSocketName) : FVector::ZeroVector;
}



/////////////////////////////////////
//INPUT HOOKS

void ASBaseWeapon::RequestStartFire()
{
	if (CanFire() && CurrentState == EWeaponState::Idle)
	{
		CurrentState = EWeaponState::Firing;
		bIsFiring = true;

		/*if (OwningCharacter && OwningCharacter->IsLocallyControlled() && Crosshair)
		{
			Crosshair->OnFirePressed();
		}*/
		if (!bFireOnRelease)
		{
			StartFiring();
		}
	}
}

void ASBaseWeapon::RequestStopFire()
{	
	if (bIsFiring)
	{
		CurrentState = EWeaponState::Idle;
		bIsFiring = false;

		/*if (OwningCharacter && OwningCharacter->IsLocallyControlled() && Crosshair)
		{
			Crosshair->OnFireReleased();
		}*/
		if (!bFireOnRelease)
		{
			StopFiring();
		}
		else
		{
			StartFiring();
		}
	}

}

void ASBaseWeapon::ToggleFiringMode()
{
	if (CurrentState == EWeaponState::Idle)
	{
		uint8 NewMode = CurrentFiringMode + 1;
		if (WeaponData.FireModes.IsValidIndex(NewMode))
		{
			CurrentFiringMode = NewMode;
		}
		else
		{
			CurrentFiringMode = 0;
		}

		if (ToggleFireModeSound)
		{
			UGameplayStatics::SpawnSoundAttached(ToggleFireModeSound, WeaponMesh);
		}
	}
}

void ASBaseWeapon::RequestStartAim()
{
	/*if (OwningCharacter && OwningCharacter->IsLocallyControlled() && Crosshair)
	{
		Crosshair->OnAimPressed();
	}*/
	//should have a canAim here...
	bIsAiming = true;
}

void ASBaseWeapon::RequestStopAim()
{
	/*if (OwningCharacter && OwningCharacter->IsLocallyControlled() && Crosshair)
	{
		Crosshair->OnAimReleased();
	}*/
	bIsAiming = false;
}

void ASBaseWeapon::RequestReload()
{
	if (CanReload() && CurrentState == EWeaponState::Idle)
	{
		CurrentState = EWeaponState::Reloading;
		
		StartReload();
	}
}


/////////////////////////////////////
//IMPLEMENTATION

void ASBaseWeapon::StartFiring()
{
	//TODO- use in first delay.  dont want player to be able to spam fire button to 
	//fire faster than time between shots.

	const FFiringMode& CurrentMode = GetCurrentFiringMode();

	uint8 bShouldLoop = CurrentMode.FireMode == EFireMode::Auto ? true : false;

	GetWorldTimerManager().SetTimer(FiringTimer, this, &ASBaseWeapon::Fire, CurrentMode.TimeBetweenShots, bShouldLoop, 0.f);

	//TODO- particle effects and sounds
	if (CurrentMode.FireSound)
	{
		FireAC = UGameplayStatics::SpawnSoundAttached(CurrentMode.FireSound, WeaponMesh);
	}
}

void ASBaseWeapon::StopFiring()
{
	GetWorldTimerManager().ClearTimer(FiringTimer);

	const FFiringMode& CurrentMode = GetCurrentFiringMode();

	//only want to fade out if it is looping.  single shot sounds or burst should finish playing
	if (FireAC && CurrentMode.FireSound->IsLooping())
	{
		FireAC->FadeOut(.1, 0.f);
	}
	if (CurrentMode.FireSoundEnd)
	{
		UGameplayStatics::SpawnSoundAttached(CurrentMode.FireSoundEnd, WeaponMesh);
	}
}

void ASBaseWeapon::StartReload()
{
	if (Role < ROLE_Authority)
	{
		Server_StartReload();
	}		

	bIsReloading = true;

	SimulateReload();

}


void ASBaseWeapon::Reload()
{
	//TODO dont need to duplicate this... just need the ui to show it.
	int32 ClipDifference = WeaponData.MaxAmmoInMag - CurrentAmmoInMag;
	int32 AmmoConsumed = FMath::Min(ClipDifference, CurrentAmmo);
	CurrentAmmoInMag += AmmoConsumed;

	CurrentAmmo -= AmmoConsumed;

	CurrentState = EWeaponState::Idle;

	OnStopReload.Broadcast(true);

	if (Role == ROLE_Authority)
	{
		bIsReloading = false;
	}
	
}

void ASBaseWeapon::OnRep_IsReloading()
{
	if (bIsReloading)
	{
		SimulateReload();
	}
	
}

void ASBaseWeapon::SimulateReload()
{
	//TODO..make default reload delay exposed
	float ReloadDuration = 1.5f;
	if (ReloadAnim)
	{
		ReloadDuration = PlayAnimation(ReloadAnim);
	}

	OnStartReload.Broadcast(ReloadDuration);

	if (Role == ROLE_Authority || OwningCharacter->IsLocallyControlled())
	{
		GetWorldTimerManager().SetTimer(ReloadTimer, this, &ASBaseWeapon::Reload, ReloadDuration, false, ReloadDuration);
	}	
}

void ASBaseWeapon::OnEquip()
{
	if (OwningCharacter)
	{
		//play weapon equip anim

		PlayAnimation(WeaponEquipAnim);
		
		if (EquipSound)
		{
			UGameplayStatics::SpawnSoundAttached(EquipSound, WeaponMesh);
		}

		if (OwningCharacter->IsLocallyControlled())
		{
			APlayerController* PC = Cast<APlayerController>(OwningCharacter->GetController());
			if (PC)
			{
				/*
				Crosshair = CreateWidget<UCrosshairWidget>(PC, WeaponData.CrosshairClass);
				if (Crosshair)
				{
				UE_LOG(LogTemp, Warning, TEXT("Adding crosshair to viewport"))
				Crosshair->AddToViewport(-999);
				}*/
			}
		}
	}


}

void ASBaseWeapon::OnUnequip()
{
	if (OwningCharacter && OwningCharacter->IsLocallyControlled())
	{
		/*if (Crosshair)
		{
			Crosshair->RemoveFromParent();
		}*/
	}

	RequestStopFire();
}

void ASBaseWeapon::Fire()
{
	UseAmmo();

	if (!CanFire())
	{
		RequestStopFire();
	}
}

bool ASBaseWeapon::CanFire()
{
	if (CurrentAmmoInMag > 0)
	{
		return true;
	}
	return false;
}

bool ASBaseWeapon::CanReload()
{
	if (CurrentAmmo > 0)
	{
		return true;
	}
	return false;
}



void ASBaseWeapon::UseAmmo()
{
	if (Role < ROLE_Authority)
	{
		Server_UseAmmo();
	}
	CurrentAmmoInMag--;
}

void ASBaseWeapon::Server_StartReload_Implementation()
{
	StartReload();
}

bool ASBaseWeapon::Server_StartReload_Validate()
{
	return true;
}

void ASBaseWeapon::Server_UseAmmo_Implementation()
{
	UseAmmo();
}

bool ASBaseWeapon::Server_UseAmmo_Validate()
{
	return true;
}


/////////////////////////////////////
//UTILITIES

bool ASBaseWeapon::GetShootDirection(FVector& OutLocation, FVector& OutDirection)
{
	/*
	if (GetOwner())
	{
		FRotator OutRotation;
		GetOwner()->GetActorEyesViewPoint(OutLocation, OutRotation);
		OutDirection = OutRotation.Vector();

		return true;
	}*/
	
	
	APlayerController* const PC = Cast<APlayerController>(GetInstigatorController());

	if (PC && PC->IsLocalController())
	{
		int32 ViewportSizeX;
		int32 ViewportSizeY;

		PC->GetViewportSize(ViewportSizeX, ViewportSizeY);

		PC->DeprojectScreenPositionToWorld(
			ViewportSizeX*.5,
			ViewportSizeY*.5,
			OutLocation,
			OutDirection);

		return true;

	}
	return false;
}

float ASBaseWeapon::PlayAnimation(UAnimMontage* AnimToPlay)
{
	if (!AnimToPlay) { return 0.f; }

	if (AnimToPlay)
	{
		UAnimInstance* AnimInstance = OwningCharacter->GetMesh()->GetAnimInstance();
		if (AnimInstance)
		{
			return AnimInstance->Montage_Play(AnimToPlay);
		}
	}
	return 0.f;
}

const FFiringMode& ASBaseWeapon::GetCurrentFiringMode()
{
	return WeaponData.FireModes[CurrentFiringMode];
}
