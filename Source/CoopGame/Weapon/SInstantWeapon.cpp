// Fill out your copyright notice in the Description page of Project Settings.

#include "SInstantWeapon.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Engine/World.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Components/SkeletalMeshComponent.h"
#include "../Effects/SImpactEffect.h"
#include "../CoopGame.h"

ASInstantWeapon::ASInstantWeapon()
{
	RoundsPerFire = 1;
}

void ASInstantWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ASInstantWeapon, HitScan, COND_SkipOwner);
}

void ASInstantWeapon::Fire()
{
	Super::Fire();

	FVector OutLocation;
	FVector OutDirection;

	const FFiringMode& CurrentMode = GetCurrentFiringMode();

	if (GetShootDirection(OutLocation, OutDirection))
	{
		FHitResult OutHit;

		UE_LOG(LogTemp, Warning, TEXT("Location: %s"),*OutLocation.ToString())
		UE_LOG(LogTemp, Warning, TEXT("Direction: %s"),*OutDirection.ToString())

		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(this);
		QueryParams.AddIgnoredActor(GetOwner());
		QueryParams.bReturnPhysicalMaterial = true;
		QueryParams.bTraceComplex = true;

		for (uint8 i = 0; i < RoundsPerFire; ++i)
		{
			FVector ShootDirection;
			CalculateShootDirection(ShootDirection, OutDirection, CurrentMode);

			GetWorld()->LineTraceSingleByChannel(OutHit, OutLocation, ShootDirection* 10000000.f, COLLISION_WEAPON, QueryParams);

			//DrawDebugLine(GetWorld(), OutLocation, OutDirection * 10000000.f, FColor::Blue, false, 1.f);

			UPhysicalMaterial* HitPhysMat = OutHit.PhysMaterial.Get();
			EPhysicalSurface HitSurfaceType = UPhysicalMaterial::DetermineSurfaceType(HitPhysMat);

			ProcessHit(OutHit.GetActor(), FHitScanTrace(HitSurfaceType, OutHit.Location, OutHit.ImpactNormal, OutHit.BoneName));
		}
	}
}

void ASInstantWeapon::ProcessHit(AActor* HitActor, const FHitScanTrace& HitScanTrace)
{
	if (Role < ROLE_Authority)
	{
		ServerProcessHit(HitActor, HitScanTrace);
	}

	if (Role == ROLE_Authority)
	{
		if (HitActor)
		{
			//apply damage
			FHitResult HitResult;
			HitResult.ImpactPoint = HitScanTrace.TraceTo;
			HitResult.Normal = HitScanTrace.HitNormal;
			HitResult.BoneName = HitScanTrace.BoneName;

			UGameplayStatics::ApplyPointDamage(HitActor, WeaponData.BaseDamage, FVector::ZeroVector, HitResult, GetInstigatorController(), this, UDamageType::StaticClass());
		}
		HitScan = HitScanTrace;
	}


	SimulateShot(HitScanTrace);
}

void ASInstantWeapon::OnRep_HitScanTrace()
{
	SimulateShot(HitScan);
}

void ASInstantWeapon::SimulateShot(const FHitScanTrace& ShotHitScan)
{
	SpawnImpactEffect(ShotHitScan);

	if (TrailParticle)
	{
		UParticleSystemComponent* UPSC = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TrailParticle, GetWeaponMuzzleLocation());
		if (UPSC)
		{
			FName BulletTrailEnd = TEXT("BeamEnd");
			UPSC->SetVectorParameter(BulletTrailEnd, ShotHitScan.TraceTo);
		}
	}
	if (MuzzleParticle)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleParticle, WeaponMesh, MuzzleSocketName, FVector::ZeroVector, FRotator::ZeroRotator, EAttachLocation::KeepRelativeOffset, true);
	}
}

void ASInstantWeapon::SpawnImpactEffect(const FHitScanTrace& ShotHitScan)
{
	if (ImpactEffectClass)
	{
		FTransform Transform = FTransform(ShotHitScan.HitNormal.Rotation(), ShotHitScan.TraceTo);
		ASImpactEffect* IE = Cast<ASImpactEffect>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, ImpactEffectClass, Transform));
		if (IE)
		{
			IE->HitScan = ShotHitScan;
		}
		UGameplayStatics::FinishSpawningActor(IE, Transform);

	}
}

void ASInstantWeapon::CalculateShootDirection(FVector& ShootDirection, const FVector& BaseDirection, const FFiringMode& FiringMode)
{
	//generate random direction based on a cone around BaseDirection
	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);

	const float ConeHalfAngle = FMath::DegreesToRadians(bIsAiming ? FiringMode.SpreadAngle / 5 : FiringMode.SpreadAngle);

	ShootDirection = WeaponRandomStream.VRandCone(BaseDirection, ConeHalfAngle, ConeHalfAngle);
}

void ASInstantWeapon::ServerProcessHit_Implementation(AActor* HitActor, const FHitScanTrace& ShotHitScan)
{
	ProcessHit(HitActor, ShotHitScan);
}

bool ASInstantWeapon::ServerProcessHit_Validate(AActor* HitActor, const FHitScanTrace& ShotHitScan)
{
	return true;
}
