// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/SBaseWeapon.h"
#include "SInstantWeapon.generated.h"

USTRUCT(BlueprintType)
struct FHitScanTrace
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
		TEnumAsByte<EPhysicalSurface> SurfaceType;

	UPROPERTY(BlueprintReadWrite)
		FVector_NetQuantize TraceTo;

	UPROPERTY(BlueprintReadWrite)
		FVector_NetQuantize HitNormal;

	UPROPERTY(BlueprintReadWrite)
		FName BoneName;

	FHitScanTrace()
	{

	}

	FHitScanTrace(TEnumAsByte<EPhysicalSurface> Surface, FVector_NetQuantize TraceHit, FVector_NetQuantize ImpactNormal, FName Bone)
	{
		SurfaceType = Surface;
		TraceTo = TraceHit;
		HitNormal = ImpactNormal;
		BoneName = Bone;
	}
};

/**
 * 
 */
UCLASS(abstract)
class COOPGAME_API ASInstantWeapon : public ASBaseWeapon
{
	GENERATED_BODY()
	
public:
	ASInstantWeapon();

	virtual void Fire() override;

protected:
	/*Called after line trace on client.  Should apply damage, initiate simulate fire.*/
	virtual void ProcessHit(AActor* HitActor, const FHitScanTrace& HitScan);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerProcessHit(AActor* HitActor, const FHitScanTrace& HitScan);

	UFUNCTION()
	void OnRep_HitScanTrace();

	/*Play effects/sounds etc to simulate weapon fire*/
	virtual void SimulateShot(const FHitScanTrace& HitScan);

	virtual void SpawnImpactEffect(const FHitScanTrace& HitScan);

	/**
	* Calculates the final shoot direction, taking into account if the weapon is ADS, spread etc.
	*/
	virtual void CalculateShootDirection(FVector& ShootDirection, const FVector& BaseDirection, const FFiringMode& FiringMode);

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class UParticleSystem* TrailParticle;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class UParticleSystem* MuzzleParticle;

	/*How many rounds to fire per fire instance.  IE a shotgun may fire 6 at a time.*/
	UPROPERTY(EditDefaultsOnly, Category = "WeaponData")
	uint8 RoundsPerFire;

	UPROPERTY(ReplicatedUsing = OnRep_HitScanTrace)
	FHitScanTrace HitScan;

};

