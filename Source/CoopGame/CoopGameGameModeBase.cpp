// Fill out your copyright notice in the Description page of Project Settings.

#include "CoopGameGameModeBase.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "UI/SHUD.h"
#include "Game/SGameState.h"


ACoopGameGameModeBase::ACoopGameGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = 1.0f;

	//GameStateClass = ACGameState::StaticClass();
	HUDClass = ASHUD::StaticClass();
	CurrentWave = 0;
	TimeBetweenWaves = 2.f;

}

void ACoopGameGameModeBase::HandleKill(AController* KillingPlayer, AController* KilledPlayer, APawn* KilledPawn)
{
	OnPawnKilled.Broadcast(KilledPawn, KillingPlayer);

	if (KillingPlayer && KillingPlayer->PlayerState)
	{
		/*
		ACPlayerState* PS = Cast<ACPlayerState>(KillingPlayer->PlayerState);
		if (PS)
		{
			PS->AddPlayerKills(1);
		}*/

	}
}

void ACoopGameGameModeBase::StartPlay()
{
	Super::StartPlay();

	PrepareForNextWave();
}

void ACoopGameGameModeBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	CheckWaveState();

	CheckPlayersAlive();
}

void ACoopGameGameModeBase::SpawnBotTimerElapsed()
{
	SpawnNewBot();

	NumBotsToSpawn--;

	if (NumBotsToSpawn <= 0)
	{
		EndWave();
	}
}

void ACoopGameGameModeBase::StartWave()
{
	CurrentWave++;

	NumBotsToSpawn = CurrentWave * 3;

	GetWorldTimerManager().SetTimer(BotSpawnTimer, this, &ACoopGameGameModeBase::SpawnBotTimerElapsed, 1.0f, true, 0.f);
	GetWorldTimerManager().ClearTimer(TimerHandle_NextWaveStart);

	SetWaveState(EWaveState::WaveInProgress);
}

void ACoopGameGameModeBase::EndWave()
{
	GetWorldTimerManager().ClearTimer(BotSpawnTimer);
	SetWaveState(EWaveState::WaitingToComplete);
}

void ACoopGameGameModeBase::PrepareForNextWave()
{
	GetWorldTimerManager().SetTimer(TimerHandle_NextWaveStart, this, &ACoopGameGameModeBase::StartWave, TimeBetweenWaves, false, TimeBetweenWaves);

	RespawnDeadPlayers();

	SetWaveState(EWaveState::WaitingToStart);
}

void ACoopGameGameModeBase::CheckWaveState()
{
	uint8 bIsPreparingForWave = GetWorldTimerManager().IsTimerActive(TimerHandle_NextWaveStart);

	if (NumBotsToSpawn > 0 || bIsPreparingForWave) { return; }

	uint8 bStillBots = false;
	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It)
	{
		APawn* TestPawn = It->Get();
		if (TestPawn && !TestPawn->IsPlayerControlled())
		{
			bStillBots = true;
			break;
		}
	}
	if (!bStillBots)
	{
		SetWaveState(EWaveState::WaveComplete);
		PrepareForNextWave();
	}
}

void ACoopGameGameModeBase::CheckPlayersAlive()
{
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		APlayerController* PC = It->Get();
		if (PC && PC->GetPawn())
		{
			//player still has pawn (IE still alive)
			return;
			/*
			ABaseCraft* CB = Cast<ABaseCraft>(PC->GetPawn());
			if (CB)
			{

			}*/
		}
	}

	EndGame();
}

void ACoopGameGameModeBase::EndGame()
{
	//do other things, like show UI
	EndWave();
	SetWaveState(EWaveState::GameOver);
}

void ACoopGameGameModeBase::SetWaveState(EWaveState NewState)
{
	ASGameState* GS = GetGameState<ASGameState>();
	if (ensureAlways(GS))
	{
		GS->SetWaveState(NewState);
	}
}

void ACoopGameGameModeBase::RespawnDeadPlayers()
{
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		APlayerController* PC = It->Get();
		if (PC && !PC->GetPawn())
		{
			RestartPlayer(PC);
		}
	}
}


