// Fill out your copyright notice in the Description page of Project Settings.

#include "STrackerBot.h"
#include "Components/StaticMeshComponent.h"
#include "kismet/GameplayStatics.h"
#include "../Player/SCharacter.h"
#include "AI/Navigation/NavigationSystem.h"
#include "AI/Navigation/NavigationPath.h"
#include "DrawDebugHelpers.h"
#include "../Components/SHealthComponent.h"
#include "Components/SphereComponent.h"
#include "Net/UnrealNetwork.h"
#include "Sound/SoundCue.h"
#include "../CoopGame.h"
#include "Engine/World.h"
#include "Materials/MaterialInstanceDynamic.h"



// Sets default values
ASTrackerBot::ASTrackerBot()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bUseVelocityChange = true;
	MovementForce = 600.f;
	RequiredDistanceToTarget = 100.f;

	bExploded = false;
	ExplosionRadius = 200.f;

	ExplosionDamage = 50.f;
	MaxBotPowerLevel = 4;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCanEverAffectNavigation(false);
	MeshComp->SetSimulatePhysics(true);

	RootComponent = MeshComp;

	HealthComp = CreateDefaultSubobject<USHealthComponent>(TEXT("HealthComp"));

	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->SetSphereRadius(ExplosionRadius);
	SphereComp->SetupAttachment(RootComponent);

	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	SphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	SphereCompOuter = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCompOuter"));
	SphereCompOuter->SetSphereRadius(ExplosionRadius*3.f);
	SphereCompOuter->SetupAttachment(RootComponent);

	SphereCompOuter->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereCompOuter->SetCollisionObjectType(TRACKER_PROXIMITY);
	SphereCompOuter->SetCollisionResponseToAllChannels(ECR_Ignore);
	SphereCompOuter->SetCollisionResponseToChannel(TRACKER_PROXIMITY, ECR_Overlap);

	
}

void ASTrackerBot::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASTrackerBot, NumBotsOverlapping);
	
}

// Called when the game starts or when spawned
void ASTrackerBot::BeginPlay()
{
	Super::BeginPlay();
	
	if (!MatInstance)
	{
		MatInstance = MeshComp->CreateAndSetMaterialInstanceDynamicFromMaterial(0, MeshComp->GetMaterial(0));
	}

	if (Role == ROLE_Authority)
	{
		TArray<AActor*> OverlappedBots;
		SphereCompOuter->GetOverlappingActors(OverlappedBots);

		NumBotsOverlapping = OverlappedBots.Num();
		NextPathPoint = GetNextPathPoint();
	}

	HandleNearbyBots();

	HealthComp->OnHealthChanged.AddDynamic(this, &ASTrackerBot::HandleTakeDamage);

}

void ASTrackerBot::HandleTakeDamage(class USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, class AActor* DamageCauser)
{
	//pulse material
	
	if (MatInstance)
	{
		MatInstance->SetScalarParameterValue("LastTimeDamageTaken", GetWorld()->TimeSeconds);
	}

	if (Health <= 0)
	{
		SelfDestruct();
	}
	
	
}

FVector ASTrackerBot::GetNextPathPoint()
{
	ACharacter* const Pawn = UGameplayStatics::GetPlayerCharacter(this, 0);
	if (Pawn)
	{
		UNavigationPath* NavPath = UNavigationSystem::FindPathToActorSynchronously(this, GetActorLocation(), Pawn);
		if (NavPath->PathPoints.Num() > 1)
		{
			return NavPath->PathPoints[1];
		}
	}
	
	return GetActorLocation();
	
}

void ASTrackerBot::SelfDestruct()
{
	if (bExploded)
	{
		return;
	}
	if (ExplosionEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation());
	}
	UGameplayStatics::PlaySoundAtLocation(this, ExplosionSound, GetActorLocation());
	
	bExploded = true;
	MeshComp->SetVisibility(false);
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	if (Role == ROLE_Authority)
	{
		TArray<AActor*> IgnoredActors;
		IgnoredActors.Add(this);

		UGameplayStatics::ApplyRadialDamage(this, ExplosionDamage*(NumBotsOverlapping+1), GetActorLocation(), ExplosionRadius, nullptr, IgnoredActors, this, GetInstigatorController(), true);

		SetLifeSpan(2.f);
	}

}

void ASTrackerBot::DamageSelf()
{
	UGameplayStatics::ApplyDamage(this, 20, GetInstigatorController(), this, nullptr);
}

// Called every frame
void ASTrackerBot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Role == ROLE_Authority && !bExploded)
	{
		float DistanceToTarget = (GetActorLocation() - NextPathPoint).Size();

		if (DistanceToTarget <= RequiredDistanceToTarget)
		{
			NextPathPoint = GetNextPathPoint();
		}
		else
		{
			FVector ForceDirection = NextPathPoint - GetActorLocation();
			ForceDirection.Normalize();
			ForceDirection *= MovementForce;
			MeshComp->AddForce(ForceDirection, NAME_None, bUseVelocityChange);

			DrawDebugDirectionalArrow(GetWorld(), GetActorLocation(), GetActorLocation() + ForceDirection, 32.f, FColor::Yellow, false, 0.f, 0, 1.f);
		}

		DrawDebugSphere(GetWorld(), NextPathPoint, 20.f, 12, FColor::Yellow, false, 0.f, 1.f);
	}
}

void ASTrackerBot::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (!bStartedSelfDestruct && !bExploded)
	{
		ASCharacter* PlayerPawn = Cast<ASCharacter>(OtherActor);
		if (PlayerPawn)
		{
			if (Role == ROLE_Authority)
			{
				//starts self destruct
				GetWorldTimerManager().SetTimer(TimerHandle_SelfDamage, this, &ASTrackerBot::DamageSelf, 0.5f, true, 0.0f);
			}
			bStartedSelfDestruct = true;

			UGameplayStatics::SpawnSoundAttached(SelfDestructSound, RootComponent);
		}
		
		ASTrackerBot* OtherBot = Cast<ASTrackerBot>(OtherActor);
		if(Role == ROLE_Authority && OtherBot)
		{
			NumBotsOverlapping++;
			HandleNearbyBots();
		}
	}
}

void ASTrackerBot::NotifyActorEndOverlap(AActor* OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);

	if (Role == ROLE_Authority)
	{
		if (OtherActor && OtherActor->IsA<ASTrackerBot>())
		{
			NumBotsOverlapping--;
			HandleNearbyBots();
		}
	}
}

void ASTrackerBot::OnRep_NumBotsOverlapping()
{
	HandleNearbyBots();
}

void ASTrackerBot::HandleNearbyBots()
{
	if (MatInstance)
	{
		MatInstance->SetScalarParameterValue("PowerLevelAlpha", (float)NumBotsOverlapping/(float)MaxBotPowerLevel);
	}
}