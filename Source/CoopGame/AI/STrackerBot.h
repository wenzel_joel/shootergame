// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "STrackerBot.generated.h"

UCLASS()
class COOPGAME_API ASTrackerBot : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASTrackerBot();

protected:
	UPROPERTY(VisibleDefaultsOnly, Category = "Setup")
	class UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleDefaultsOnly, Category = "Setup")
	class USHealthComponent* HealthComp;

	UPROPERTY(VisibleDefaultsOnly, Category = "Setup")
	class USphereComponent* SphereComp;

	UPROPERTY(VisibleDefaultsOnly, Category = "Setup")
	class USphereComponent* SphereCompOuter;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void HandleTakeDamage(class USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, class AActor* DamageCauser);

	

	FVector GetNextPathPoint();

	//Next point in navigation path
	FVector NextPathPoint;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	float MovementForce;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	bool bUseVelocityChange;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	float RequiredDistanceToTarget;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class UParticleSystem* ExplosionEffect;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class USoundCue* SelfDestructSound;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class USoundCue* ExplosionSound;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	int32 MaxBotPowerLevel;

	UPROPERTY(ReplicatedUsing = OnRep_NumBotsOverlapping)
	int32 NumBotsOverlapping;

	UFUNCTION()
	void OnRep_NumBotsOverlapping();

	void HandleNearbyBots();

	//Dynamic Material to pulse on damage
	class UMaterialInstanceDynamic* MatInstance;

	void SelfDestruct();

	bool bExploded;

	bool bStartedSelfDestruct;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	float ExplosionRadius;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	float ExplosionDamage;

	FTimerHandle TimerHandle_SelfDamage;

	UFUNCTION()
	void DamageSelf();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	virtual void NotifyActorEndOverlap(AActor* OtherActor) override;

};
