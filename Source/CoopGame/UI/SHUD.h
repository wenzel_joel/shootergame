// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SHUD.generated.h"

class UUserWidget;

namespace EHudState
{
	enum Type
	{
		Warmup, //match hasn't started and no input is allowed
		Gameplay, //no extra windows up, just main gameplay widget.  IE the player is moving around and shooting
		Inventory, //inventory is up, other widgets hidden
		InGameMenu, //player is in options menu, gameplay paused for player
		Spectating
	};
}

//work around as there isn't a good way to expose input mode as a selectable option
UENUM(BlueprintType)
enum class EInputMode : uint8 
{
	GameOnly	UMETA(DisplayName = "GameOnly"),
	GameAndUI	UMETA(DisplayName = "GameAndUI"),
	UIOnly		UMETA(DisplayName = "UIOnly")
};

USTRUCT(BlueprintType)
struct FWidgetInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	bool bShowMouseCursor;

	UPROPERTY(EditDefaultsOnly)
	EInputMode InputMode;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> WidgetClass;

	/*  dont really want to include UserWidget in the header just for this overload
	FORCEINLINE bool operator!() const
	{
		return WidgetClass;
	}*/

	FWidgetInfo()
	{
		bShowMouseCursor = false;
		InputMode = EInputMode::GameOnly;
		WidgetClass = nullptr;
	}
};

USTRUCT()
struct FWorldTextIndicator
{
	GENERATED_BODY();

	/*actual text to display*/
	FString Text;

	/*Size and color of the text*/
	float TextSize;
	FColor TextColor;

	/*Current/Starting location and Ending Location*/
	FVector2D Location;
	FVector2D EndingLocation;

	FWorldTextIndicator()
	{

	}

};

/**
 * 
 */
UCLASS()
class COOPGAME_API ASHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	ASHUD();

	virtual void BeginPlay() override;

	virtual void ShowDamageIndicator(float Damage, FVector2D ScreenLocation, float TextSize, FColor TextColor);

	virtual void DrawHUD() override;

	virtual void ShowGameplayWidget();
	virtual void ShowInventoryWidget();

protected:
	/*Handles any drawing of active indicators*/
	virtual void DrawIndicators();

	virtual void Tick(float DeltaSeconds) override;

	virtual void DisplayWidget(class UUserWidget* WidgetToAdd, const FWidgetInfo& WidgetInfo);

	virtual void HideWidget(class UUserWidget* WidgetToHide);


	///////////////////////////////////
	//STATE
	
	virtual EHudState::Type GetDefaultState();

	virtual void UpdateState(EHudState::Type DesiredState);

	virtual void StartGameplayState();
	virtual void StartInventoryState();
	virtual void StartGameMenuState();

	virtual void EndGameplayState();
	virtual void EndInventoryState();
	virtual void EndGameMenuState();

protected:
	EHudState::Type CurrentState;

	/////////////////////////////////
	//WIDGETS

	UPROPERTY(EditDefaultsOnly, Category = "HUD")
	FWidgetInfo GameplayWidgetInfo;

	TWeakObjectPtr<UUserWidget> GameplayWidget;

	UPROPERTY(EditDefaultsOnly, Category = "HUD")
	FWidgetInfo InventoryWidgetInfo;

	TWeakObjectPtr<UUserWidget> InventoryWidget;


	/////////////////////////////////
	//STYLE

	UPROPERTY(EditDefaultsOnly, Category = "HUD")
	class UFont* IndicatorFont;

	FFontRenderInfo ShadowedFont;

	/*Currently Displayed indicators (Damage indicators, health updates etc*/
	UPROPERTY()
	TArray<FWorldTextIndicator> ActiveIndicators;
	
	
};
