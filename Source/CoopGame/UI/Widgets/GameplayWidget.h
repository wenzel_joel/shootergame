// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameplayWidget.generated.h"

/**
 * 
 */
UCLASS()
class COOPGAME_API UGameplayWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize() override;

protected:
	UPROPERTY(meta = (BindWidget))
	class UInventoryWidget* HotBarWidget;

	UPROPERTY(meta = (BindWidget))
	class UProgressBar* PB_Health;
	
	
};
