// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InteractableWidget.generated.h"

DECLARE_DELEGATE_OneParam(FOnInteractFinished, bool /* bFinishedSuccessfully */);

/**
 * 
 */
UCLASS(abstract)
class COOPGAME_API UInteractableWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UInteractableWidget(const FObjectInitializer& ObjectInitializer);

	void InitInteract(const FText& Message, float ConfirmDuration, bool bShowTimer);

	void OnInteractPressed(FOnInteractFinished Callback);

	void OnInteractFinished();

	void StopInteractImmediately();

protected:
	virtual bool Initialize();

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime);

	void SetInteractText(const FText& Text);

protected:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* Txt_Interact;

	UPROPERTY(meta = (BindWidget))
	class UProgressBar* PB_ConfirmInteract;

	FOnInteractFinished CurrentCallback;

	uint8 bIsInteracting;

	float InteractDuration;
	float StartInteractTime;

	

	
	
};
