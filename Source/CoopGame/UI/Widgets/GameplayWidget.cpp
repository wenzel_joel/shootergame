// Fill out your copyright notice in the Description page of Project Settings.

#include "GameplayWidget.h"




bool UGameplayWidget::Initialize()
{
	if (!ensure(HotBarWidget)) { return false; }
	if (!ensure(PB_Health)) { return false; }

	return Super::Initialize();
}
