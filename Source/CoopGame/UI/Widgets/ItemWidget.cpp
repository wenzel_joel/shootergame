// Fill out your copyright notice in the Description page of Project Settings.

#include "ItemWidget.h"
#include "Components/Image.h"
#include "Components/Border.h"
#include "../../Inventory/ItemDataAsset.h"
#include "../../Weapon/SBaseWeapon.h"

UItemWidget::UItemWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}


void UItemWidget::OnSelect()
{
	SetRenderTranslation(FVector2D(0, -10));
	ReceiveOnSelect();
}

void UItemWidget::OnUnselect()
{
	SetRenderTranslation(FVector2D(0, 0));
	ReceiveOnUnselect();
}

void UItemWidget::OnActionStarted(float Duration)
{
	ReceiveOnActionStarted(Duration);
}

void UItemWidget::OnActionCompleted(uint8 bSuccessful)
{
	ReceiveOnActionCompleted(bSuccessful);
}


void UItemWidget::Setup(ASInventoryItem* Item, uint8 Index)
{
	InventoryIndex = Index;

	if (Item)
	{
		SetItem(Item);
	}
	else
	{
		SetAsEmpty();
	}
}

void UItemWidget::SetAsEmpty()
{
	if (CurrentItem)
	{
		CurrentItem = nullptr;
	}
	B_Background->SetRenderOpacity(0.f);
}

bool UItemWidget::Initialize()
{
	if (!Super::Initialize()) { return false; }

	if (!ensure(Img_Item)) return false;
		
	if (!ensure(Txt_Quantity)) return false;

	if (!ensure(B_Background)) return false;


	return true;
}

void UItemWidget::NativeConstruct()
{
	Super::NativeConstruct();

}

void UItemWidget::NativePreConstruct()
{
	Super::NativePreConstruct();
}

void UItemWidget::SetItem(ASInventoryItem* Item)
{
	if (!Item) { return; }
	
	CurrentItem = Item;

	if (Img_Item && Item->GetDataAsset())
	{
		Img_Item->SetBrushFromTexture(Item->GetDataAsset()->ItemTexture);
		B_Background->SetRenderOpacity(1.f);
	}

	Item->OnSelected.AddDynamic(this, &UItemWidget::OnSelect);
	Item->OnUnselected.AddDynamic(this, &UItemWidget::OnUnselect);

	//check if we have a weapon and bind reload delegates
	ASBaseWeapon* Weapon = Cast<ASBaseWeapon>(Item);
	if (Weapon)
	{
		Weapon->OnStartReload.AddDynamic(this, &UItemWidget::OnActionStarted);
		Weapon->OnStopReload.AddDynamic(this, &UItemWidget::OnActionCompleted);
	}

	if (CurrentItem->IsSelected())
	{
		OnSelect();
	}
}
