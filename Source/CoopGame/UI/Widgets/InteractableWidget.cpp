// Fill out your copyright notice in the Description page of Project Settings.

#include "InteractableWidget.h"
#include "Components/TextBlock.h"
#include "Components/ProgressBar.h"
#include "Engine/World.h"


UInteractableWidget::UInteractableWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	InteractDuration = 0.f;
	bIsInteracting = false;
}

void UInteractableWidget::SetInteractText(const FText& Text)
{
	Txt_Interact->SetText(Text);
}

void UInteractableWidget::InitInteract(const FText& Message, float ConfirmDuration, bool bShowTimer)
{
	SetInteractText(Message);
	InteractDuration = ConfirmDuration;

	if (bShowTimer)
	{
		PB_ConfirmInteract->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
		PB_ConfirmInteract->SetVisibility(ESlateVisibility::Hidden);
	}
}

void UInteractableWidget::OnInteractPressed(FOnInteractFinished Callback)
{
	CurrentCallback = Callback;
	
	StartInteractTime = GetWorld()->GetTimeSeconds();

	//if we have duration, start tick logic.  else just call complete delegate
	if (InteractDuration > 0.f)
	{
		bIsInteracting = true;
	}
	else
	{
		OnInteractFinished();
	}
	
}

void UInteractableWidget::OnInteractFinished()
{
	bIsInteracting = false;
	CurrentCallback.ExecuteIfBound(true);
}

void UInteractableWidget::StopInteractImmediately()
{
	bIsInteracting = false;
	CurrentCallback.ExecuteIfBound(false);

	PB_ConfirmInteract->SetPercent(0.f);
}

bool UInteractableWidget::Initialize()
{
	if (!Super::Initialize()) { return false; }

	if (!ensure(Txt_Interact)) return false;

	if (!ensure(PB_ConfirmInteract)) return false;

	return true;
}

void UInteractableWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (bIsInteracting)
	{
		float CurrentTime = GetWorld()->GetTimeSeconds();
		if (CurrentTime >= StartInteractTime + InteractDuration)
		{
			OnInteractFinished();
		}
		else
		{
			float CurrentFill = PB_ConfirmInteract->Percent;
			float DeltaFill = InDeltaTime / InteractDuration;
			PB_ConfirmInteract->SetPercent(CurrentFill + DeltaFill);
		}
	}
}
