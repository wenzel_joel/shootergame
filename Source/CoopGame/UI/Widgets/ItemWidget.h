// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ItemWidget.generated.h"

class ASInventoryItem;

/**
 * 
 */
UCLASS()
class COOPGAME_API UItemWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UItemWidget(const FObjectInitializer& ObjectInitializer);

	/**
	 * Updates this widget to reflect the passed item
	 */
	void Setup(ASInventoryItem* Item, uint8 Index);

	

protected:
	virtual bool Initialize();

	virtual void NativeConstruct();

	virtual void NativePreConstruct();

	/**
	* Clears the active item from this widget.
	* Should be used when the item is removed from inventory or swapped positions
	*/
	void SetAsEmpty();

	virtual void SetItem(ASInventoryItem* Item);

	UFUNCTION()
	virtual void OnSelect();

	UFUNCTION()
	virtual void OnUnselect();

	UFUNCTION()
	virtual void OnActionStarted(float Duration);

	UFUNCTION()
	virtual void OnActionCompleted(uint8 bSuccessful);

	UFUNCTION(BlueprintImplementableEvent, Category = "Item", meta = (DisplayName = "OnSelect"))
	void ReceiveOnSelect();

	UFUNCTION(BlueprintImplementableEvent, Category = "Item", meta = (DisplayName = "OnUnselect"))
	void ReceiveOnUnselect();

	UFUNCTION(BlueprintImplementableEvent, Category = "Item", meta = (DisplayName = "OnActionStarted"))
	void ReceiveOnActionStarted(float Duration);

	UFUNCTION(BlueprintImplementableEvent, Category = "Item", meta = (DisplayName = "OnActionCompleted"))
	void ReceiveOnActionCompleted(bool bSuccessful);

protected:
	UPROPERTY(meta = (BindWidget))
	class UImage* Img_Item;

	UPROPERTY(meta = (BindWidget))
	class UBorder* B_Background;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* Txt_Quantity;

	UPROPERTY(BlueprintReadOnly, Category = "Item")
	ASInventoryItem* CurrentItem;

	UPROPERTY(BlueprintReadOnly, Category = "Item")
	uint8 InventoryIndex;
	
	
};
