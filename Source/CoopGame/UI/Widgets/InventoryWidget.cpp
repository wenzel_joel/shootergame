// Fill out your copyright notice in the Description page of Project Settings.

#include "InventoryWidget.h"
#include "Components/HorizontalBoxSlot.h"
#include "Components/HorizontalBox.h"
#include "ItemWidget.h"
#include "../../Player/SCharacter.h"
#include "../../Components/InventoryComponent.h"
#include "../../Inventory/SInventoryItem.h"
#include "TimerManager.h"
#include "Engine/World.h"




UInventoryWidget::UInventoryWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

bool UInventoryWidget::Initialize()
{
	if (!Super::Initialize()) { return false; }

	if (!ensure(PW_WeaponPanel)) return false;

	return true;
}

void UInventoryWidget::NativeConstruct()
{
	Super::NativeConstruct();

	OwningCharacter = Cast<ASCharacter>(GetOwningPlayer()->GetPawn());

	SpawnInventory();

	if (OwningCharacter->IsLocallyControlled())
	{
		OwningCharacter->GetInventory()->OnInventorySlotUpdated.AddDynamic(this, &UInventoryWidget::UpdateInventorySlot);
	}

}

void UInventoryWidget::UpdateInventorySlot(uint8 SlotUpdated, ASInventoryItem* NewItem)
{
	//TODO.. kind of forced to rebuild entire panel as PanelWidget.ReplaceChild and ReplaceChildAt doesnt work
	//as expected
	SpawnInventory();
}

void UInventoryWidget::SpawnInventory()
{
	if (!ItemWidgetClass) { return; }

	TArray<FItemSlot>& Items = OwningCharacter->GetInventory()->GetItems();
	PW_WeaponPanel->ClearChildren();

	for (uint8 i = 0; i < Items.Num() ; ++i)
	{
		const FItemSlot& Slot = Items[i];

		UItemWidget* ItemW = CreateWidget<UItemWidget>(Cast<APlayerController>(OwningCharacter->GetController()), ItemWidgetClass);
		if (ItemW)
		{
			ItemW->Setup(Slot.ItemActor, i);
			AddWidgetToInventory(ItemW);
		}
	}

	ReceiveSpawnInventory();
}

void UInventoryWidget::AddWidgetToInventory(UUserWidget* WidgetToAdd)
{
	UHorizontalBoxSlot* HBSlot = PW_WeaponPanel->AddChildToHorizontalBox(WidgetToAdd);
	if (HBSlot)
	{
		HBSlot->SetSize(FSlateChildSize(ESlateSizeRule::Fill));
	}
}

