// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InventoryWidget.generated.h"

/**
 * 
 */
UCLASS()
class COOPGAME_API UInventoryWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UInventoryWidget(const FObjectInitializer& ObjectInitializer);

protected:
	virtual bool Initialize();

	virtual void NativeConstruct();

	UFUNCTION()
	void UpdateInventorySlot(uint8 SlotUpdated, class ASInventoryItem* NewItem);

	void SpawnInventory();

	UFUNCTION(BlueprintImplementableEvent, Category = "Inventory", meta = (DisplayName = "SpawnInventory"))
	void ReceiveSpawnInventory();

	virtual void AddWidgetToInventory(UUserWidget* WidgetToAdd);

protected:
	UPROPERTY(meta = (BindWidget), BlueprintReadWrite)
	class UHorizontalBox* PW_WeaponPanel;

	UPROPERTY()
	class ASCharacter* OwningCharacter;

	UPROPERTY(EditDefaultsOnly, Category = "InventoryWidget")
	TSubclassOf<UUserWidget> EmptySlotWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = "InventoryWidget")
	TSubclassOf<class UItemWidget> ItemWidgetClass;


};

