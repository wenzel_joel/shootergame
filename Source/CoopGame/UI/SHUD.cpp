// Fill out your copyright notice in the Description page of Project Settings.

#include "SHUD.h"
#include "Engine/Canvas.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"


ASHUD::ASHUD()
{
	ShadowedFont.bEnableShadow = true;

}

void ASHUD::BeginPlay()
{
	Super::BeginPlay();

	ensure(GameplayWidgetInfo.WidgetClass);
	ensure(InventoryWidgetInfo.WidgetClass);

	UpdateState(GetDefaultState());
}



void ASHUD::ShowDamageIndicator(float Damage, FVector2D ScreenLocation, float TextSize, FColor TextColor)
{
	FWorldTextIndicator DamageIndicator;
	DamageIndicator.Text = FString::FromInt((int32)Damage);
	DamageIndicator.Location = ScreenLocation;
	DamageIndicator.TextSize = TextSize;
	DamageIndicator.TextColor = TextColor;

	//randomize ending location
	int32 RandomX = FMath::RandRange(-50, 50);
	int32 RandomY = FMath::RandRange(0, 50);
	DamageIndicator.EndingLocation = FVector2D(ScreenLocation.X + 0, ScreenLocation.Y - RandomY);

	ActiveIndicators.Add(DamageIndicator);
}

void ASHUD::DrawHUD()
{
	Super::DrawHUD();

	DrawIndicators();

}

void ASHUD::ShowGameplayWidget()
{
	UpdateState(EHudState::Gameplay);
}

void ASHUD::ShowInventoryWidget()
{
	UpdateState(EHudState::Inventory);
}

void ASHUD::DrawIndicators()
{
	//draw active indicators
	for (int32 Index = ActiveIndicators.Num() - 1; Index >= 0; --Index)
	{
		FWorldTextIndicator& I = ActiveIndicators[Index];

		if (I.Location.Equals(I.EndingLocation, .01f))
		{
			ActiveIndicators.RemoveAt(Index);
		}
		else
		{
			if (IndicatorFont)
			{
				IndicatorFont->LegacyFontSize = I.TextSize;
			}
			FCanvasTextItem TextItem(FVector2D::ZeroVector, FText::FromString(I.Text), IndicatorFont, I.TextColor);
			TextItem.EnableShadow(FLinearColor::Black);

			I.Location = FMath::Vector2DInterpTo(I.Location, I.EndingLocation, LastHUDRenderTime, .01f);

			Canvas->DrawItem(TextItem, I.Location.X, I.Location.Y);
		}

	}
}

void ASHUD::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ASHUD::DisplayWidget(UUserWidget* WidgetToAdd, const FWidgetInfo& WidgetInfo)
{
	if (!WidgetToAdd->IsInViewport())
	{
		WidgetToAdd->AddToViewport();
	}

	PlayerOwner->bShowMouseCursor = WidgetInfo.bShowMouseCursor;

	//maybe move this to a player controller util function
	switch (WidgetInfo.InputMode)
	{
	case EInputMode::GameAndUI:
		PlayerOwner->SetInputMode(FInputModeGameAndUI());
		break;
	case EInputMode::GameOnly:
		PlayerOwner->SetInputMode(FInputModeGameOnly());
		break;
	case EInputMode::UIOnly:
		PlayerOwner->SetInputMode(FInputModeUIOnly());
		break;
	}

	WidgetToAdd->SetVisibility(ESlateVisibility::Visible);

}

void ASHUD::HideWidget(class UUserWidget* WidgetToHide)
{
	if (!WidgetToHide) { return; }
	WidgetToHide->SetVisibility(ESlateVisibility::Hidden);
}

EHudState::Type ASHUD::GetDefaultState()
{
	return EHudState::Gameplay;
}

void ASHUD::UpdateState(EHudState::Type DesiredState)
{
	if (CurrentState == DesiredState) { return; }

	switch (CurrentState)
	{
	case EHudState::Gameplay:
		EndGameplayState();
		break;
	case EHudState::InGameMenu:
		EndGameMenuState();
		break;
	case EHudState::Inventory:
		EndInventoryState();
		break;
	}

	switch (DesiredState)
	{
	case EHudState::Gameplay:
		StartGameplayState();
		break;
	case EHudState::InGameMenu:
		StartGameMenuState();
		break;
	case EHudState::Inventory:
		StartInventoryState();
		break;
	}

	CurrentState = DesiredState;
}

void ASHUD::StartGameplayState()
{
	if (!GameplayWidgetInfo.WidgetClass) { return; }

	if (!GameplayWidget.IsValid())
	{
		GameplayWidget = CreateWidget<UUserWidget>(PlayerOwner, GameplayWidgetInfo.WidgetClass);
		GameplayWidget->AddToViewport();
	}

	DisplayWidget(GameplayWidget.Get(), GameplayWidgetInfo);
}

void ASHUD::StartInventoryState()
{
	if (!InventoryWidgetInfo.WidgetClass) { return; }

	if (!InventoryWidget.IsValid())
	{
		InventoryWidget = CreateWidget<UUserWidget>(PlayerOwner, InventoryWidgetInfo.WidgetClass);
		InventoryWidget->AddToViewport();
	}

	DisplayWidget(InventoryWidget.Get(), InventoryWidgetInfo);
}

void ASHUD::StartGameMenuState()
{

}

void ASHUD::EndGameplayState()
{
	if (!GameplayWidget.IsValid()) { return; }
	HideWidget(GameplayWidget.Get());
}

void ASHUD::EndInventoryState()
{
	if (!InventoryWidget.IsValid()) { return; }
	HideWidget(InventoryWidget.Get());
}

void ASHUD::EndGameMenuState()
{

}

