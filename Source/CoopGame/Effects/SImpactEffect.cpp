// Fill out your copyright notice in the Description page of Project Settings.

#include "SImpactEffect.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "../CoopGame.h"




// Sets default values
ASImpactEffect::ASImpactEffect()
{
	/** this doesnt seem to be working.. just calling destroy at end of post init */
	bAutoDestroyWhenFinished = true;
}

void ASImpactEffect::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	EPhysicalSurface HitSurfaceType = HitScan.SurfaceType;

	// show particles
	UParticleSystem* ImpactFX = GetImpactFX(HitSurfaceType);
	if (ImpactFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, ImpactFX, GetActorLocation(), GetActorRotation());
	}

	// play sound
	USoundCue* ImpactSound = GetImpactSound(HitSurfaceType);
	if (ImpactSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ImpactSound, GetActorLocation());
	}

	Destroy();
}

UParticleSystem* ASImpactEffect::GetImpactFX(TEnumAsByte<EPhysicalSurface> SurfaceType) const
{
	UParticleSystem* ImpactFX = NULL;

	switch (SurfaceType)
	{
	case SHOOTER_SURFACE_Concrete:	ImpactFX = ConcreteFX; break;
	case SHOOTER_SURFACE_Dirt:		ImpactFX = DirtFX; break;
	case SHOOTER_SURFACE_Water:		ImpactFX = WaterFX; break;
	case SHOOTER_SURFACE_Metal:		ImpactFX = MetalFX; break;
	case SHOOTER_SURFACE_Wood:		ImpactFX = WoodFX; break;
	case SHOOTER_SURFACE_Flesh:		ImpactFX = FleshFX; break;
	case SHOOTER_SURFACE_Shield:	ImpactFX = ShieldFX; break;
	default:						ImpactFX = DefaultFX; break;
	}

	return ImpactFX;
}

USoundCue* ASImpactEffect::GetImpactSound(TEnumAsByte<EPhysicalSurface> SurfaceType) const
{
	USoundCue* ImpactSound = NULL;

	switch (SurfaceType)
	{
	case SHOOTER_SURFACE_Concrete:	ImpactSound = ConcreteSound; break;
	case SHOOTER_SURFACE_Dirt:		ImpactSound = DirtSound; break;
	case SHOOTER_SURFACE_Water:		ImpactSound = WaterSound; break;
	case SHOOTER_SURFACE_Metal:		ImpactSound = MetalSound; break;
	case SHOOTER_SURFACE_Wood:		ImpactSound = WoodSound; break;
	case SHOOTER_SURFACE_Flesh:		ImpactSound = FleshSound; break;
	case SHOOTER_SURFACE_Shield:		ImpactSound = ShieldSound; break;
	default:						ImpactSound = DefaultSound; break;
	}

	return ImpactSound;
}
