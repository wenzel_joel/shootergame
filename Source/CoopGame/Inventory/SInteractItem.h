// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SInteractItem.generated.h"

UCLASS()
class COOPGAME_API ASInteractItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASInteractItem();

	virtual float GetInteractDuration();

	virtual void OnInteract(class APawn* InteractingPawn);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor);

	UPROPERTY(VisibleAnywhere, Category = "Setup")
	class USphereComponent* SphereComp;

	UPROPERTY(VisibleAnywhere, Category = "Setup")
	class UStaticMeshComponent* MeshComp;

	UPROPERTY(EditAnywhere, Category = "Interact")
	float InteractDuration;

	/** If true, the player will automatically trigger OnInteract when they overlap */
	UPROPERTY(EditDefaultsOnly, Category = "Interact")
	bool bCanAutoInteract;

	UPROPERTY(EditAnywhere, Category = "Interact")
	bool bDestroyPostInteract;

	uint8 bCollected;

	////////////////////////////////
	//EFFECTS AND SOUND

	UPROPERTY(EditDefaultsOnly, Category = "Interact")
	class USoundBase* InteractSound;

protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "Interact", meta = (DisplayName = "OnInteract"))
	void ReceiveOnInteract(class APawn* InteractingPawn);

	UFUNCTION(BlueprintImplementableEvent, Category = "Interact", meta = (DisplayName = "OnLocalInteract"))
	void ReceiveLocalOnInteract(class APawn* InteractingPawn);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


};
