// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Inventory/SInteractItem.h"
#include "SItemDrop.generated.h"


class ASInventoryItem;

/**
 * 
 */
UCLASS()
class COOPGAME_API ASItemDrop : public ASInteractItem
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ItemDrop")
	FORCEINLINE ASInventoryItem* GetItem() { return Item; }

	void SetItem(ASInventoryItem* NewItem);

	virtual void OnInteract(class APawn* InteractingPawn) override;
protected:
	UPROPERTY(ReplicatedUsing = OnRep_Item, BlueprintReadOnly, Category = "ItemDrop")
	ASInventoryItem* Item;

	UFUNCTION()
	void OnRep_Item();
	
	
};
