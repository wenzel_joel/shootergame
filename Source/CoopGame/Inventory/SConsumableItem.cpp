// Fill out your copyright notice in the Description page of Project Settings.

#include "SConsumableItem.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "Particles/ParticleSystem.h"
#include "../Player/SCharacter.h"

void ASConsumableItem::OnSelect()
{
	Super::OnSelect();
}

void ASConsumableItem::OnUnselect()
{
	Super::OnUnselect();
}

void ASConsumableItem::OnPickup()
{

}

void ASConsumableItem::OnDrop()
{

}

void ASConsumableItem::OnUsePressed()
{
	if (Role < ROLE_Authority)
	{
		Server_OnUsePressed();
	}

	else if (Quantity > 0)
	{
		--Quantity;
		ReceiveOnUsedPressed();
		SimulateItemUse();
		
		if (Quantity <= 0)
		{
			//destroy.. need to tell inventory this was destroyed..
		}
	}


}

void ASConsumableItem::OnUseReleased()
{
	//maybe add a timer and an exposed uproperty(needDuration) and return how long use was held down.
}

void ASConsumableItem::BeginPlay()
{
	Super::BeginPlay();

	Quantity = 3;
}

void ASConsumableItem::Server_OnUsePressed_Implementation()
{
	OnUsePressed();
}

bool ASConsumableItem::Server_OnUsePressed_Validate()
{
	return true;
}

void ASConsumableItem::SimulateItemUse_Implementation()
{
	if (!ensure(OwningCharacter)) { return; }

	if (SpawnParticle)
	{
		UGameplayStatics::SpawnEmitterAttached(SpawnParticle, OwningCharacter->GetRootComponent());
	}
	if (SpawnSound)
	{
		UGameplayStatics::SpawnSoundAttached(SpawnSound, OwningCharacter->GetRootComponent());
	}
}
