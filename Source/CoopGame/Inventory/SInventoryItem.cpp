// Fill out your copyright notice in the Description page of Project Settings.

#include "SInventoryItem.h"
#include "GameFramework/Character.h"
#include "../UI/Widgets/ItemWidget.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"
#include "ItemDataAsset.h"


// Sets default values
ASInventoryItem::ASInventoryItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;
	bIsSelected = false;

	MaxQuantity = 1;
}

void ASInventoryItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ASInventoryItem, Quantity, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(ASInventoryItem, bIsSelected, COND_OwnerOnly);
}

void ASInventoryItem::OnSelect()
{
	OnSelected.Broadcast();
	bIsSelected = true;
}

void ASInventoryItem::OnUnselect()
{
	OnUnselected.Broadcast();
	bIsSelected = false;
}

void ASInventoryItem::OnDrop()
{
	if (DropSound)
	{		
		UGameplayStatics::SpawnSoundAtLocation(this, DropSound, OwningCharacter->GetActorLocation());
	}
	bIsSelected = false;
	
}

void ASInventoryItem::SetOwner(AActor* NewOwner)
{
	Super::SetOwner(NewOwner);
	OwningCharacter = Cast<ACharacter>(NewOwner);
}

void ASInventoryItem::OnRep_Owner()
{
	Super::OnRep_Owner();
	OwningCharacter = Cast<ACharacter>(GetOwner());
}

int32 ASInventoryItem::UpdateQuantity(int32 DeltaQuantity)
{
	if (Role < ROLE_Authority) { return 0; }

	int32 Remainder = DeltaQuantity - (MaxQuantity - Quantity);
	
	Quantity = FMath::Clamp(Quantity + DeltaQuantity, 0, MaxQuantity);
	
	return Remainder;
}

// Called when the game starts or when spawned
void ASInventoryItem::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void ASInventoryItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

