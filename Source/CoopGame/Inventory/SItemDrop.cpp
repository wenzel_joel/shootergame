// Fill out your copyright notice in the Description page of Project Settings.

#include "SItemDrop.h"
#include "../Player/SCharacter.h"
#include "SInventoryItem.h"
#include "Components/StaticMeshComponent.h"
#include "Net/UnrealNetwork.h"
#include "../Components/InventoryComponent.h"


void ASItemDrop::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASItemDrop, Item);
}

void ASItemDrop::SetItem(ASInventoryItem* NewItem)
{
	if (Role < ROLE_Authority) { return; }
	Item = NewItem;
	MeshComp->SetStaticMesh(NewItem->GetDropMesh());
	Item->SetActorHiddenInGame(true);
}

void ASItemDrop::OnInteract(class APawn* InteractingPawn)
{
	if (Role == ROLE_Authority)
	{
		ASCharacter* Character = Cast<ASCharacter>(InteractingPawn);
		if (Character && Item)
		{
			Item->SetOwner(Character);
			if (Character->GetInventory())
			{
				Character->GetInventory()->AddItem(Item);
			}
		}
	}
	
	Super::OnInteract(InteractingPawn);
}

void ASItemDrop::OnRep_Item()
{
	MeshComp->SetStaticMesh(Item->GetDropMesh());
}
