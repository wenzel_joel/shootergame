// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SInventoryItem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSelected);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUnselected);

UCLASS(abstract)
class COOPGAME_API ASInventoryItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASInventoryItem();

	FOnSelected OnSelected;
	FOnUnselected OnUnselected;

	/**
	 * Return the data asset, containing UI and persisted info
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Item")
	FORCEINLINE class UItemDataAsset* GetDataAsset()
	{
		return DataAsset;
	}

	/**
	* Called when item is selected from inventory
	*/
	virtual void OnSelect();

	/**
	* Called when item is deselected from inventory
	*/
	virtual void OnUnselect();

	/**
	* Called when item is picked up from the world
	*/
	virtual void OnPickup() PURE_VIRTUAL(ASInventoryItem::OnPickup, ;);

	/**
	* Called when item is dropped into the world
	*/
	virtual void OnDrop();

	/**
	* Called when item is pressed after being selected from inventory
	*/
	virtual void OnUsePressed() PURE_VIRTUAL(ASInventoryItem::OnUsePressed, ;);

	/**
	* Called when item is released after being selected from inventory
	*/
	virtual void OnUseReleased() PURE_VIRTUAL(ASInventoryItem::OnUseReleased, ;);

	virtual void SetOwner(AActor* NewOwner) override;

	virtual void OnRep_Owner() override;


	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Item")
	FORCEINLINE bool IsSelected() { return bIsSelected;	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Item")
	FORCEINLINE int32 GetQuantity() { return Quantity; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Item")
	FORCEINLINE UStaticMesh* GetDropMesh() { return DropMesh; }


	/**
	 * Increments or decrements the quantity on this item.  
	 * Returns - the remainder,  if the max quantity is 5 and the resulting quantity is 7
	 * then the quantity will be 5 with 2 returned.
	 */
	UFUNCTION(BlueprintCallable, Category = "Item")
	int32 UpdateQuantity(int32 DeltaQuantity);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Item")
	class UStaticMesh* DropMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Item")
	class UItemDataAsset* DataAsset;

	UPROPERTY(EditDefaultsOnly, Category = "Item")
	class USoundBase* DropSound;

	UPROPERTY(EditDefaultsOnly, Category = "Item")
	int32 MaxQuantity;

	UPROPERTY(BlueprintReadOnly, Category = "Item")
	class ACharacter* OwningCharacter;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Item")
	int32 Quantity;

	/** May need to replicate this.. */
	UPROPERTY(Replicated)
	uint8 bIsSelected;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
