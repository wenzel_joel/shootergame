// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ItemDataAsset.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class COOPGAME_API UItemDataAsset : public UDataAsset
{
	GENERATED_BODY()
	
public:
	/*Name of the ship.  This will be what is displayed on the UI*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	FString ItemName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	TSubclassOf<class ASInventoryItem> ItemClass;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ItemData")
	class UTexture2D* ItemTexture;
};
