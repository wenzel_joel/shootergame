// Fill out your copyright notice in the Description page of Project Settings.

#include "SInteractItem.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "../Player/SCharacter.h"


// Sets default values
ASInteractItem::ASInteractItem()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->SetSphereRadius(75.f);
	RootComponent = SphereComp;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetupAttachment(GetRootComponent());
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	InteractDuration = 1.f;

	bDestroyPostInteract = true;
	bCanAutoInteract = false;

	bReplicates = true;

	bCollected = false;

}

// Called when the game starts or when spawned
void ASInteractItem::BeginPlay()
{
	Super::BeginPlay();

}

void ASInteractItem::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (!bCanAutoInteract || bCollected) { return; }

	if (ASCharacter* PlayerC = Cast<ASCharacter>(OtherActor))
	{		
		bCollected = true;
		OnInteract(PlayerC);
	}
}

// Called every frame
void ASInteractItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


float ASInteractItem::GetInteractDuration()
{
	return InteractDuration;
}

void ASInteractItem::OnInteract(class APawn* InteractingPawn)
{
	if (Role == ROLE_Authority)
	{
		ReceiveOnInteract(InteractingPawn);
	}

	if(InteractingPawn->IsLocallyControlled())
	{
		ReceiveLocalOnInteract(InteractingPawn);
	}

	if (InteractSound)
	{
		UGameplayStatics::SpawnSoundAtLocation(this, InteractSound, GetActorLocation());
	}

	if (bDestroyPostInteract && Role == ROLE_Authority)
	{
		Destroy();
	}
}

