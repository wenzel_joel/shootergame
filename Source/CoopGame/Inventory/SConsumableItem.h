// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Inventory/SInventoryItem.h"
#include "SConsumableItem.generated.h"

/**
 * 
 */
UCLASS(abstract)
class COOPGAME_API ASConsumableItem : public ASInventoryItem
{
	GENERATED_BODY()
	
public:
	/** Inventory Item Interface */
	virtual void OnSelect() override;
	virtual void OnUnselect() override;
	virtual void OnPickup() override;
	virtual void OnDrop() override;
	virtual void OnUsePressed() override;
	virtual void OnUseReleased() override;

protected:
	virtual void BeginPlay() override;

	/**
	 * Blueprint logic for using an item.  Only called on the server
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "Consumable", meta = (DisplayName = "OnUsePressed"))
	void ReceiveOnUsedPressed();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnUsePressed();

	UFUNCTION(NetMulticast, Unreliable)
	void SimulateItemUse();

protected:
	/*The actor that is physically spawned */
	UPROPERTY(EditDefaultsOnly, Category = "Consumable")
	TSubclassOf<AActor> SpawnableActorClass;

	UPROPERTY(EditDefaultsOnly, Category = "Consumable")
	class UParticleSystem* SpawnParticle;

	UPROPERTY(EditDefaultsOnly, Category = "Consumable")
	class USoundBase* SpawnSound;

	
	
};
