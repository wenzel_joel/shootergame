// Fill out your copyright notice in the Description page of Project Settings.

#include "DamagableActor.h"
#include "Components/SHealthComponent.h"


// Sets default values
ADamagableActor::ADamagableActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));

	HealthComponent = CreateDefaultSubobject<USHealthComponent>(TEXT("HealthComp"));

	bIsDead = false;

	SetReplicates(true);
	SetReplicateMovement(true);

}

// Called when the game starts or when spawned
void ADamagableActor::BeginPlay()
{
	Super::BeginPlay();

	HealthComponent->OnHealthChanged.AddDynamic(this, &ADamagableActor::OnHealthChanged);
	
}

void ADamagableActor::OnHealthChanged(class USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, class AActor* DamageCauser)
{
	if (Health <= 0.f && !bIsDead)
	{
		bIsDead = true;
		OnNoHealth();
	}
}

