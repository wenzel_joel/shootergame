// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DamagableActor.generated.h"

UCLASS()
class COOPGAME_API ADamagableActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADamagableActor();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Setup")
	class USHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Setup")
	class UStaticMeshComponent* MeshComponent;

	bool bIsDead;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnHealthChanged(class USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, class AActor* DamageCauser);

	UFUNCTION(BlueprintImplementableEvent, Category = "Health")
	void OnNoHealth();
	
	
};
