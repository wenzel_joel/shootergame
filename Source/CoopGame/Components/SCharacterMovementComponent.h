// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "SCharacterMovementComponent.generated.h"

USTRUCT()
struct FCoverRequest
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FVector_NetQuantize WallNormal;

	UPROPERTY()
	FVector_NetQuantize WallLocation;

	FCoverRequest(FVector Normal, FVector Location)
	{
		WallNormal = Normal;
		WallLocation = Location;
	}

	FCoverRequest()
	{

	}
};

/**
 * 
 */
UCLASS()
class COOPGAME_API USCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
public:
	USCharacterMovementComponent();
	
protected:
	/** The trace distance used to determine if a player can enter cover */
	UPROPERTY(EditDefaultsOnly, Category = "Character Movement: Cover")
	float CoverTraceDistance;

	/** How big of a sphere to trace for cover, the bigger the radius the more likely you will enter cover. */
	UPROPERTY(EditDefaultsOnly, Category = "Character Movement: Cover")
	float CoverTraceSphereRadius;

	/** How far from the wall your character will be during cover */
	UPROPERTY(EditDefaultsOnly, Category = "Character Movement: Cover")
	float CoverOffsetDistance;

	/** How long it takes the mesh to move into cover */
	UPROPERTY(EditDefaultsOnly, Category = "Character Movement: Cover")
	float TimeToEnterCover;

	bool bIsInCover;

	/** The Normal of the wall we are currently covering behind */
	UPROPERTY(ReplicatedUsing = OnRep_CoverRequest)
	FCoverRequest CoverRequest;

protected:
	virtual void BeginPlay() override;

	void OnEnterCover();

	void OnLeaveCover();
	
	UFUNCTION()
	void OnRep_CoverRequest();

	void EnterCover(const FCoverRequest& Request);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_EnterCover(const FCoverRequest& Request);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_LeaveCover();


public:

	/**
	* Attempts to put the character into a cover position
	*/
	void RequestEnterCover();

	void RequestLeaveCover();


};
