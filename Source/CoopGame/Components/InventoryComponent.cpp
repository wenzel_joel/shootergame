// Fill out your copyright notice in the Description page of Project Settings.

#include "InventoryComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Animation/AnimInstance.h"
#include "Net/UnrealNetwork.h"
#include "../Inventory/SInventoryItem.h"
#include "Components/MeshComponent.h"
#include "../Inventory/SItemDrop.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	bReplicates = true;

	SelectedItemIndex = -1;
	SelectedItemAttachSocket = TEXT("WeaponSocket");
}

void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UInventoryComponent, ItemSlots, COND_OwnerOnly);
	DOREPLIFETIME(UInventoryComponent, SelectedItem);
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwnerRole() == ROLE_Authority)
	{
		OwningMesh = Cast<UMeshComponent>(GetOwner()->GetComponentByClass(UMeshComponent::StaticClass()));
		check(OwningMesh);

		//index based for loop so we can pass index around
		for (uint8 i = 0; i < ItemSlots.Num(); ++i)
		{
			FItemSlot& Slot = ItemSlots[i];

			if (Slot.DefaultItemClass)
			{
				ASInventoryItem* Item = SpawnItem(Slot);

				SetSlot(i, Item);
			}
		}
	}

}


ASInventoryItem* UInventoryComponent::SpawnItem(FItemSlot& Slot)
{
	if (Slot.DefaultItemClass)
	{
		ASInventoryItem* ItemActor = Cast<ASInventoryItem>(
			UGameplayStatics::BeginDeferredActorSpawnFromClass(
				this,
				Slot.DefaultItemClass,
				FTransform(),
				ESpawnActorCollisionHandlingMethod::AlwaysSpawn,
				GetOwner()
			)
			);

		if (ItemActor)
		{
			ItemActor->Instigator = Cast<APawn>(GetOwner());
			UGameplayStatics::FinishSpawningActor(ItemActor, FTransform());

			Slot.ItemActor = ItemActor;			
		}

		return ItemActor;
	}
	return nullptr;
}

void UInventoryComponent::AttachItem(const FItemSlot& Slot)
{
	if (OwningMesh && Slot.ItemActor)
	{
		Slot.ItemActor->AttachToComponent(OwningMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, SelectedItemAttachSocket);
		Slot.ItemActor->SetActorHiddenInGame(false);
	}
}

void UInventoryComponent::OnRep_ItemSlots(const TArray<FItemSlot>& OldItems)
{
	//iterate through our new item slots compare to old and notify as needed
	for (uint8 i = 0; i < ItemSlots.Num(); ++i)
	{
		const FItemSlot& OldItem = OldItems[i];
		const FItemSlot& NewItem = ItemSlots[i];

		if (NewItem != OldItem)
		{
			OnInventorySlotUpdated.Broadcast(i, NewItem.ItemActor);
		}
	}
}


void UInventoryComponent::OnRep_SelectedItem(ASInventoryItem* PreviousItem)
{
	if (PreviousItem)
	{
		PreviousItem->OnUnselect();
	}
	if (SelectedItem)
	{
		SelectedItem->OnSelect();
	}
}

// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}



///////////////////////////////////////
//SELECTION

void UInventoryComponent::SelectItem(uint8 ItemIndex)
{
	if (GetOwnerRole() < ROLE_Authority) { return; }
	
	if (!IsValidSelection(ItemIndex)) { return; }
	
	ASInventoryItem* PreviousItem = nullptr;

	if (SelectedItem)
	{
		PreviousItem = SelectedItem;
		UnselectItem();
	}

	FItemSlot& Slot = ItemSlots[ItemIndex];
	
	AttachItem(Slot);

	OnItemSelected.Broadcast(ItemIndex, Slot.ItemActor);

	SelectedItemIndex = ItemIndex;

	SelectedItem = Slot.ItemActor;

	OnRep_SelectedItem(PreviousItem);
}

void UInventoryComponent::UnselectItem()
{
	if (GetOwnerRole() < ROLE_Authority) { return; }

	if (SelectedItem && ItemSlots.IsValidIndex(SelectedItemIndex))
	{
		FItemSlot& Slot = ItemSlots[SelectedItemIndex];

		SelectedItem->SetActorHiddenInGame(true);

		OnItemUnselected.Broadcast(SelectedItemIndex, Slot.ItemActor);

		SelectedItem = nullptr;
	}

}

bool UInventoryComponent::IsValidSelection(uint8 ItemIndex)
{
	return
		(SelectedItemIndex != ItemIndex) &&
		(ItemSlots.IsValidIndex(ItemIndex)) &&
		(ItemSlots[ItemIndex].ItemActor)
		;
}


bool UInventoryComponent::IsValidDrop(ASInventoryItem* ItemToDrop, int32 Quantity)
{
	if (GetOwnerRole() < ROLE_Authority) { return false; }
	int32 CurrentQuantity = ItemToDrop->GetQuantity();
	return false;
}

void UInventoryComponent::SetSlot(uint8 Index, ASInventoryItem* Item)
{
	if (GetOwnerRole() < ROLE_Authority) { return; }
	if (ItemSlots.IsValidIndex(Index))
	{
		ItemSlots[Index].ItemActor = Item;

		if (Item)
		{
			Item->SetOwner(GetOwner());
			Item->Instigator = Cast<APawn>(GetOwner());
			Item->SetActorHiddenInGame(true);
		}
		

		OnInventorySlotUpdated.Broadcast(Index, Item);
	}
}

void UInventoryComponent::SpawnDropItem(ASInventoryItem* Item)
{
	if (!ensure(ItemDropClass)) { return; }

	ASItemDrop* ItemDrop = Cast<ASItemDrop>(
		UGameplayStatics::BeginDeferredActorSpawnFromClass(
			this, 
			ItemDropClass,
			OwningMesh->GetComponentTransform(), 
			ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn
			)
		);

	ItemDrop->SetItem(Item);

	UGameplayStatics::FinishSpawningActor(ItemDrop, OwningMesh->GetComponentTransform());
}

void UInventoryComponent::SpawnDropItemFromClass(TSubclassOf<ASInventoryItem> ItemClass, int32 Quantity)
{

}

FTransform UInventoryComponent::GetDropSpawn()
{
	if (!OwningMesh) { return; }
	return FTransform();
}

///////////////////////////////////////////
//DROP

void UInventoryComponent::Server_DropItem_Implementation(uint8 ItemIndex, int32 Quantity)
{
	DropIndex(ItemIndex, Quantity);
}

bool UInventoryComponent::Server_DropItem_Validate(uint8 ItemIndex, int32 Quantity)
{
	return true;
}

void UInventoryComponent::DropIndex(uint8 ItemIndex, int32 Quantity)
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		Server_DropItem(ItemIndex, Quantity);
		return;
	}

	if (!ItemSlots.IsValidIndex(ItemIndex)) { return; }

	FItemSlot& SlotToDrop = ItemSlots[ItemIndex];

	//TODO.. actually drop the item in the world.
	ASInventoryItem* DroppedItem = SlotToDrop.ItemActor;

	//if we are dropping all of the item, so empty the slot
	if (Quantity >= DroppedItem->GetQuantity())
	{
		SetSlot(ItemIndex, nullptr);

		SpawnDropItem(DroppedItem);
		DroppedItem->OnDrop();
	}

	//else we are dropping a partial quantity
	else
	{
		DroppedItem->UpdateQuantity(-Quantity);
		SpawnDropItemFromClass(DroppedItem->GetClass(), Quantity);
	}

	
}

void UInventoryComponent::DropItem(class ASInventoryItem* Item, int32 Quantity)
{
	const uint8 Index = GetItemIndex(Item);
	if (Index < 0) { return; }

	DropIndex(Index, Quantity);
}




void UInventoryComponent::SwapIndex_Implementation(uint8 FirstIndex, uint8 SecondIndex)
{
	if (!ItemSlots.IsValidIndex(FirstIndex) || !ItemSlots.IsValidIndex(SecondIndex)) { return; }

	ItemSlots.Swap(FirstIndex, SecondIndex);

	//TODO..change this to just InventoryUpdated.
	OnInventorySlotUpdated.Broadcast(FirstIndex, nullptr);
}

bool UInventoryComponent::SwapIndex_Validate(uint8 FirstIndex, uint8 SecondIndex)
{
	return true;
}

void UInventoryComponent::AddItem_Implementation(ASInventoryItem* Item)
{
	if (!Item) { return; }
	
	for (uint8 i = 0; i < ItemSlots.Num() ; ++i)
	{
		FItemSlot& Slot = ItemSlots[i];

		if (Slot.ItemActor == nullptr)
		{
			SetSlot(i, Item);
			break;
		}
	}
}

bool UInventoryComponent::AddItem_Validate(ASInventoryItem* Item)
{
	return true;
}

//////////////////////////////////////////////
//UTIL

uint8 UInventoryComponent::GetItemIndex(const ASInventoryItem* Item)
{
	for (uint8 i = 0; i < ItemSlots.Num(); ++i)
	{
		if (ItemSlots[i].ItemActor == Item)
		{
			return i;
		}
	}
	return -1;
}

