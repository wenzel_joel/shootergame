// Fill out your copyright notice in the Description page of Project Settings.

#include "SHealthComponent.h"
#include "Net/UnrealNetwork.h"
#include "../CoopGameGameModeBase.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"


// Sets default values for this component's properties
USHealthComponent::USHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	bReplicates = true;
	bCountsAsKill = true;
	MaxHealth = 100.f;
}

void USHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(USHealthComponent, CurrentHealth);
}

// Called when the game starts
void USHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner() && GetOwnerRole() == ROLE_Authority)
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &USHealthComponent::HandleDamage);
	}

	CurrentHealth = MaxHealth;
}


void USHealthComponent::HandleDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (CurrentHealth <= 0) { return; }

	CurrentHealth = FMath::Clamp(CurrentHealth - Damage, 0.f, MaxHealth);

	if (CurrentHealth <= 0 && bCountsAsKill)
	{
		//notify game mode of death
		
		ACoopGameGameModeBase* GM = Cast<ACoopGameGameModeBase>(GetWorld()->GetAuthGameMode());
		if (GM)
		{
			APawn* Owner = Cast<APawn>(GetOwner());

			GM->HandleKill(InstigatedBy, Owner ? Owner->GetController() : nullptr, Owner ? Owner : nullptr);
		}
	}

	OnHealthChanged.Broadcast(this, CurrentHealth, -Damage, nullptr, InstigatedBy, DamageCauser);
}

void USHealthComponent::OnRep_CurrentHealth(float OldHealth)
{
	OnHealthChanged.Broadcast(this, CurrentHealth, CurrentHealth - OldHealth, nullptr, nullptr, nullptr);
}

void USHealthComponent::Heal(float DeltaHealth)
{
	if (GetOwnerRole() < ROLE_Authority) { return; }
	
	if (CurrentHealth >= 0)
	{
		CurrentHealth = FMath::Clamp(DeltaHealth + CurrentHealth, 0.f, MaxHealth);
	}
	OnHealthChanged.Broadcast(this, CurrentHealth, DeltaHealth, nullptr, nullptr, nullptr);
}
