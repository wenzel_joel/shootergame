// Fill out your copyright notice in the Description page of Project Settings.

#include "SInteractComponent.h"
#include "GameFramework/Pawn.h"
#include "Engine/World.h"
#include "../Inventory/SInteractItem.h"
#include "GameFramework/InputSettings.h"
#include "../UI/Widgets/InteractableWidget.h"

// Sets default values for this component's properties
USInteractComponent::USInteractComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bReplicates = true;

	InteractDistance = 100.f;
	bUsePawnControlRotation = true;

	bReplicates = true;

	DefaultInteractText = FText::FromString(TEXT("Interact"));

	OnInteractFinishedDelegate = FOnInteractFinished::CreateUObject(this, &USInteractComponent::OnInteractFinished);
}


// Called when the game starts
void USInteractComponent::BeginPlay()
{
	Super::BeginPlay();

	OwningPawn = Cast<APawn>(GetOwner());
	if (!ensure(OwningPawn)) { return; }

	if (OwningPawn->IsLocallyControlled())
	{
		TArray<FInputActionKeyMapping> KeyMappings;

		//TODO.. #define inputs somewhere and reference that instead
		UInputSettings::GetInputSettings()->GetActionMappingByName(TEXT("Interact"), KeyMappings);

		if (KeyMappings.IsValidIndex(0))
		{
			InteractDisplayName = KeyMappings[0].Key.GetDisplayName();
		}
		else
		{
			InteractDisplayName = DefaultInteractText;
		}
	}
}


void USInteractComponent::SetFocus(class ASInteractItem* NewItem)
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		Server_SetFocus(NewItem);
	}
	
	if (FocusedItem)
	{
		ClearFocus();
	}

	FocusedItem = NewItem;

	if (InteractWidgetClass && OwningPawn->IsLocallyControlled())
	{
		CurrentInteractWidget = CreateWidget<UInteractableWidget>(Cast<APlayerController>(OwningPawn->GetController()), InteractWidgetClass);
		if (CurrentInteractWidget)
		{
			float InteractTime = NewItem->GetInteractDuration();
			CurrentInteractWidget->InitInteract(InteractDisplayName, InteractTime, InteractTime > 0.f ? true : false);
			CurrentInteractWidget->AddToViewport();
		}
	}

	
}

void USInteractComponent::Server_SetFocus_Implementation(ASInteractItem* NewItem)
{
	SetFocus(NewItem);
}

bool USInteractComponent::Server_SetFocus_Validate(ASInteractItem* NewItem)
{
	return true;
}

void USInteractComponent::ClearFocus()
{
	FocusedItem = nullptr;
	if (CurrentInteractWidget)
	{
		CurrentInteractWidget->RemoveFromParent();
		CurrentInteractWidget = nullptr;
	}
}

void USInteractComponent::OnInteractFinished(bool bWasSuccessful)
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		Server_OnInteractFinished(bWasSuccessful);
	}
	if (bWasSuccessful)
	{
		FocusedItem->OnInteract(OwningPawn);
		
		if (CurrentInteractWidget)
		{
			CurrentInteractWidget->RemoveFromParent();
			CurrentInteractWidget = nullptr;
		}
	}
}

void USInteractComponent::Server_OnInteractFinished_Implementation(bool bWasSuccessful)
{
	OnInteractFinished(bWasSuccessful);
}

bool USInteractComponent::Server_OnInteractFinished_Validate(bool bWasSuccessful)
{
	return true;
}

// Called every frame
void USInteractComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (OwningPawn->IsLocallyControlled())
	{
		const FVector& OwnerLocation = OwningPawn->GetActorLocation();
		const FVector& OwnerRotation = bUsePawnControlRotation ? OwningPawn->GetControlRotation().Vector() : OwningPawn->GetActorForwardVector();

		FHitResult OutHit;

		if (GetWorld()->LineTraceSingleByChannel(
			OutHit,
			OwnerLocation, OwnerLocation + (OwnerRotation*InteractDistance),
			ECollisionChannel::ECC_Visibility))
		{
			ASInteractItem* Item = Cast<ASInteractItem>(OutHit.GetActor());

			//if we have a new focus item
			if (Item && Item != FocusedItem)
			{
				SetFocus(Item);
			}

			//trace succeeded but didnt hit an interact item
			else if (Item == nullptr)
			{
				ClearFocus();
			}
		}
		else
		{
			ClearFocus();
		}

	}

}

void USInteractComponent::StartInteract()
{
	if (CurrentInteractWidget && FocusedItem)
	{
		UE_LOG(LogTemp, Warning, TEXT("Requesting Start Interact"))
		CurrentInteractWidget->OnInteractPressed(OnInteractFinishedDelegate);
	}
}

void USInteractComponent::StopInteract()
{
	if (CurrentInteractWidget)
	{
		UE_LOG(LogTemp, Warning, TEXT("Requesting Stop Interact"))
		CurrentInteractWidget->StopInteractImmediately();
	}
}

