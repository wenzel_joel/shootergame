// Fill out your copyright notice in the Description page of Project Settings.

#include "SCharacterMovementComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/CapsuleComponent.h"
#include "Net/UnrealNetwork.h"




USCharacterMovementComponent::USCharacterMovementComponent()
{
	bReplicates = true;

	bIsInCover = false;
	CoverTraceDistance = 150.f;
	CoverTraceSphereRadius = 20.f;
	CoverOffsetDistance = 35.f;
	TimeToEnterCover = .2f;
}

void USCharacterMovementComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USCharacterMovementComponent, CoverRequest);
}

void USCharacterMovementComponent::BeginPlay()
{
	Super::BeginPlay();
}

void USCharacterMovementComponent::OnEnterCover()
{
	bIsInCover = true;
	CharacterOwner->bUseControllerRotationYaw = false;
	bOrientRotationToMovement = false;
	bUseControllerDesiredRotation = false;
}

void USCharacterMovementComponent::OnLeaveCover()
{
	bIsInCover = false;
	CharacterOwner->bUseControllerRotationYaw = true;
	//bOrientRotationToMovement = true;
	//bUseControllerDesiredRotation = true;
}

void USCharacterMovementComponent::OnRep_CoverRequest()
{

}

void USCharacterMovementComponent::EnterCover(const FCoverRequest& Request)
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		Server_EnterCover(Request);
	}
	
		//WATCH - might need to neutralize the z axis on current cover normal
		FVector MoveToLocation = Request.WallLocation + (Request.WallNormal*CoverOffsetDistance);

		FRotator MoveToRotation = Request.WallNormal.Rotation();

		FLatentActionInfo LatentInfo;
		LatentInfo.CallbackTarget = this;

		UKismetSystemLibrary::MoveComponentTo(CharacterOwner->GetCapsuleComponent(), MoveToLocation, MoveToRotation, false, false, TimeToEnterCover, false, EMoveComponentAction::Move, LatentInfo);
	

	OnEnterCover();
}

void USCharacterMovementComponent::Server_EnterCover_Implementation(const FCoverRequest& Request)
{
	EnterCover(Request);
}

bool USCharacterMovementComponent::Server_EnterCover_Validate(const FCoverRequest& Request)
{
	return true;
}

void USCharacterMovementComponent::Server_LeaveCover_Implementation()
{
	RequestLeaveCover();
}

bool USCharacterMovementComponent::Server_LeaveCover_Validate()
{
	return true;
}

void USCharacterMovementComponent::RequestLeaveCover()
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		Server_LeaveCover();
	}
	OnLeaveCover();
}

void USCharacterMovementComponent::RequestEnterCover()
{
	if (bIsInCover) { return; }

	FHitResult OutHit;

	const FVector& StartTrace = GetOwner()->GetActorLocation();

	FVector RelativeEndTrace = GetOwner()->GetActorForwardVector()*CoverTraceDistance;
	RelativeEndTrace.Z = 0.f;

	TArray<AActor*> IgnoreActors;

	if (UKismetSystemLibrary::SphereTraceSingle(this, StartTrace, StartTrace + RelativeEndTrace, CoverTraceSphereRadius, UEngineTypes::ConvertToTraceType(ECC_Visibility), false, IgnoreActors, EDrawDebugTrace::Persistent, OutHit, true))
	{
		EnterCover(FCoverRequest(OutHit.Normal, OutHit.Location));
	}

}
