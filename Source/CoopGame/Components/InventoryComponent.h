// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"

class ASInventoryItem;

USTRUCT(BlueprintType)
struct FItemSlot
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Inventory")
	TSubclassOf<class ASInventoryItem> DefaultItemClass;

	UPROPERTY(BlueprintReadOnly, Category = "Inventory")
	class ASInventoryItem* ItemActor;

	//override the "==" operator so that this object can be easily compared
	FORCEINLINE bool operator==(const FItemSlot &Other) const
	{
		return ItemActor == Other.ItemActor;
	}

	//override the "==" operator so that this object can be easily compared
	FORCEINLINE bool operator!=(const FItemSlot &Other) const
	{
		return ItemActor != Other.ItemActor;
	}

	FItemSlot()
	{
		DefaultItemClass = nullptr;
		ItemActor = nullptr;
	}
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnItemSelected, uint8, SlotSelected, class ASInventoryItem*, NewItem);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnItemUnselected, uint8, SlotUnselected, class ASInventoryItem*, OldItem);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FInventorySlotUpdated, uint8, SlotUpdated, class ASInventoryItem*, NewItem);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class COOPGAME_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

	FOnItemSelected OnItemSelected;
	FOnItemUnselected OnItemUnselected;

	FInventorySlotUpdated OnInventorySlotUpdated;

protected:
	UPROPERTY(ReplicatedUsing = OnRep_ItemSlots, EditDefaultsOnly, Category = "Inventory")
	TArray<FItemSlot> ItemSlots;

	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
	FName SelectedItemAttachSocket;

	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
	TSubclassOf<class ASItemDrop> ItemDropClass;

	UPROPERTY()
	class UMeshComponent* OwningMesh;

	uint8 SelectedItemIndex;

	UPROPERTY(ReplicatedUsing = OnRep_SelectedItem, BlueprintReadOnly, Category = "Inventory")
	ASInventoryItem* SelectedItem;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	ASInventoryItem* SpawnItem(FItemSlot& Slot);

	void AttachItem(const FItemSlot& Slot);

	UFUNCTION()
	void OnRep_ItemSlots(const TArray<FItemSlot>& OldItems);

	UFUNCTION()
	void OnRep_SelectedItem(ASInventoryItem* PreviousItem);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_DropItem(uint8 ItemIndex, int32 Quantity);

	virtual bool IsValidSelection(uint8 ItemIndex);

	virtual bool IsValidDrop(ASInventoryItem* ItemToDrop, int32 Quantity);

	virtual void SetSlot(uint8 Index, ASInventoryItem* Item);

	virtual void SpawnDropItem(ASInventoryItem* Item);

	virtual void SpawnDropItemFromClass(TSubclassOf<ASInventoryItem> ItemClass, int32 Quantity);

	virtual FTransform GetDropSpawn();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	FORCEINLINE void SetOwningMesh(class UMeshComponent* NewMesh) { OwningMesh = NewMesh; }

	virtual void SelectItem(uint8 ItemIndex);

	virtual void UnselectItem();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	FORCEINLINE TArray<FItemSlot>& GetItems() { return ItemSlots; }

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	virtual void DropIndex(uint8 ItemIndex, int32 Quantity = 1);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	virtual void DropItem(ASInventoryItem* Item, int32 Quantity = 1);

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Inventory")
	virtual void SwapIndex(uint8 FirstIndex, uint8 SecondIndex);

	/**
	 * Will attempt to add the item to the players inventory in the next available slot
	 */
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Inventory")
	virtual void AddItem(ASInventoryItem* Item);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	uint8 GetItemIndex(const ASInventoryItem* Item);
		
	
};
