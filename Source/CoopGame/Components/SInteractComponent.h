// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../UI/Widgets/InteractableWidget.h"
#include "SInteractComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class COOPGAME_API USInteractComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USInteractComponent();

protected:
	/** Trace distance to use when determining if an interactable object is within reach */
	UPROPERTY(EditDefaultsOnly, Category = "Interact")
	float InteractDistance;

	/** Should we use pawn control rotation to perform trace */
	UPROPERTY(EditDefaultsOnly, Category = "Interact")
	bool bUsePawnControlRotation;

	UPROPERTY(EditDefaultsOnly, Category = "Interact")
	TSubclassOf<class UInteractableWidget> InteractWidgetClass;

	/** Used in the case of no key binding found for interact */
	UPROPERTY(EditDefaultsOnly, Category = "Interact")
	FText DefaultInteractText;

	UPROPERTY()
	class UInteractableWidget* CurrentInteractWidget;

	UPROPERTY()
	class APawn* OwningPawn;

	UPROPERTY()
	class ASInteractItem* FocusedItem;

	FDelegateHandle OnInteractCompleteHandle;
	FOnInteractFinished OnInteractFinishedDelegate;


	///////////////////////////////////////
	//INPUT
	FText InteractDisplayName;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void SetFocus(class ASInteractItem* NewItem);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_SetFocus(class ASInteractItem* NewItem);

	void ClearFocus();

	void OnInteractFinished(bool bWasSuccessful);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnInteractFinished(bool bWasSuccessful);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void StartInteract();

	virtual void StopInteract();

		
	
};
